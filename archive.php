<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="wrap cf">

			<main id="main" class="cf" role="main">

				<nav aria-label="Breadcrumb" >
					<?php custom_breadcrumbs(); ?>
				</nav>
				<?php the_archive_title('<h1 class="page-title">', '</h1>'); ?>
				<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">

						<?php include(get_template_directory() . '/components/post/post-header.php'); ?>

						<section class="entry-content cf">

							<?php the_excerpt(); ?>

						</section>

						<?php include(get_template_directory() . '/components/post/post-info-comment-cat-tag.php'); ?>

					</article>

				<?php endwhile; ?>

					<?php bones_page_navi(); ?>

				<?php else : ?>

					<?php include(get_template_directory() . '/components/post/post-not-found.php'); ?>

				<?php endif; ?>

			</main>

			<?php // get_sidebar();?>

		</div>

	</div>

<?php get_footer(); ?>
