<?php
    // Set the query for one interview
    $args = array(
        'post_type'      => 'interviews',
        'post_status'    => 'publish',
        'posts_per_page' => 6,
    );
    $query_interview = new WP_Query($args);
    
    // Data for the section
    $object           = get_queried_object();
    $interviews_title = get_field('interviews-title', $object->name);
?>

<?php get_header(); ?>

	<div id="content">
		
		<section class="header-top">
			<div class="wrap">
				<nav aria-label="Breadcrumb" >
					<ul id="breadcrumbs" class="breadcrumbs">
						<li class="item-home"><a class="bread-home"  href="/"><?php _e('Accueil', 'opteven') ?></a></li>
						<li class="item-parent"><a class="bread-parent" href="/nos-metiers"><?php _e('Nos métiers', 'opteven');?></a></li>
						<li class="item-parent"><a class="bread-parent" href="/temoignages"><?php _e('Témoignages', 'opteven');?></a></li>
						<li aria-current="page" class="item-current"><strong><?php post_type_archive_title(); ?></strong></li>
					</ul>				
				</nav>
				<h1>
					<?php post_type_archive_title(); ?>
				</h1>		
			</div>
		</section>

		<main id="main" class="main">			
			<div class="wrap">	
				<h2><?= $interviews_title ?></h2>
			</div>

			<!-- Start the loop for one video testimony -->
			<?php if ($query_interview->have_posts()) : while ($query_interview->have_posts()) : $query_interview->the_post();?>
			
			<section class="interview">
				<div class="wrap">

					<div class="author-identity">
						<!-- Display the interview author's image -->
						<img src="<?= the_post_thumbnail_url('full') ?>" alt="">

						<!-- Display the interview author's name -->
						<div class="wp-signature">
							<p><?php the_title() ?></p>
						</div>
						
						<!-- Display the interview author's job -->
						<?php $job = get_field('relation_jobs_interview'); ?>
						<?php foreach ($job as $j): ?>
							<h5><?= $j->post_title ?></h5>
						<?php endforeach; ?>
					</div>

					<!-- Display the slider interview -->
					<div class="slider-container">
						<div class="slider-interview">
							<ul class="slides-interview">
								<?php while (have_rows('interview')): the_row(); ?>
									<li class="slide">								
										<h3><?= get_sub_field('question') ?></h3>
										<p><?= get_sub_field('response') ?></p>
									</li>
								<?php endwhile; ?>
							</ul>
						</div>		
					</div>	
				</div>
			</section>
			<?php endwhile; endif;?><!-- End the loop for one testimony -->	
		</main>
	</div>
<?php get_footer(); ?>
