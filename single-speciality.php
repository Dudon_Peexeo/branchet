<?php get_header(); ?>

<div id="content">

    <div id="inner-content" class="cf">

        <nav aria-label="Breadcrumb">
            <?php custom_breadcrumbs(); ?>
        </nav>

        <main id="main" role="main">
            <div class="wrap cf">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>

                            <?php include(get_template_directory() . '/components/post/post-header.php'); ?>

                            <section class="entry-content cf" itemprop="articleBody">
                                <?php
                                // the content (pretty self explanatory huh)
                                the_content();

                                
                                ?>
                            </section> <?php // end article section
                                        ?>

                            <!-- <?php include(get_template_directory() . '/components/post/post-info-comment-cat-tag.php'); ?> -->

                            <?php //comments_template();
                            ?>

                        </article> <?php // end article
                                    ?>

                    <?php endwhile; ?>

                <?php else : ?>

                    <?php include(get_template_directory() . '/components/post/post-not-found.php'); ?>

                <?php endif; ?>
                
            </div>    
            <?php include(get_template_directory() . '/include/general/offers.php'); ?>
            <?php include(get_template_directory() . '/include/general/testimony.php'); ?>
                

        </main>

        <?php // get_sidebar();
        ?>

    </div>

</div>

<?php get_footer(); ?>