<?php get_header(); ?>

	<main id="main" role="main">

		<section class="header-top error-404">
			<div class="wrap">
				<nav aria-label="Breadcrumb" >
					<?php custom_breadcrumbs(); ?>
				</nav>
				<h1>
					<?php _e('Erreur 404', 'opteven'); ?>
				</h1>
			</div>
		</section>

		<section class="wrap entry-content">
			<h2><?php _e('Oups… Cette page n’existe pas', 'opteven');?></h2>
			<p>
				<?php _e('Nous n’avons pas trouvé la page que vous cherchiez.', 'opteven') ?>
			</p>
			<p>
				<?php _e('N’hésitez pas à consulter')?> 		
			<a class="faq" href="/faq">
				<?php _e(' notre FAQ,');?>
			</a>
				<?php _e(' vous trouverez peut-être les réponses à vos questions.', 'opteven');?>
			</p>
				
			<a class="button-link" href="/"><?php _e('Retourner à l\'accueil', 'opteven');?></a>
		</section>

	</main>

<?php get_footer(); ?>
