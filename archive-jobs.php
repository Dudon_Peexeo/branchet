<?php
    // Data for the section
    $object     = get_queried_object();
    $page_title = get_field('jobs_page_title', $object->name);
    $search_bar = get_field('jobs_search_bar', $object->name);
    $button     = get_field('jobs_button', $object->name);
    $see_more   = get_field('jobs_see_more', $object->name);
?>

<?php get_header(); ?>

<main id="main" class="main">			
	
	<section class="header-top">
		<div class="wrap">
			<nav aria-label="Breadcrumb" >
				<?php custom_breadcrumbs(); ?>
			</nav>
			<h1>
				<?php post_type_archive_title(); ?>
			</h1>		
		</div>
	</section>			

	<!-- Search bar category for all jobs -->
	<?php include(get_template_directory() . '/include/jobs/archive/search-bar.php'); ?>

	<!-- Get all jobs -->
	<?php include(get_template_directory() . '/include/jobs/archive/all-jobs.php');?>

</main>

<?php get_footer(); ?>
