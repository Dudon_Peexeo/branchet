<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="20px" height="20px"
	 viewBox="0 0 28.45 20" style="enable-background:new 0 0 28.45 20;" xml:space="preserve">
<path id="youtube" fill="#FFFFFF" class="st0" d="M27.85,3.13c-0.33-1.23-1.29-2.2-2.51-2.53C23.12,0,14.22,0,14.22,0S5.33,0,3.11,0.6
	C1.88,0.93,0.92,1.9,0.59,3.13c-0.79,4.56-0.79,9.22,0,13.78c0.33,1.22,1.29,2.17,2.51,2.49C5.33,20,14.22,20,14.22,20
	s8.9,0,11.11-0.6c1.22-0.32,2.18-1.27,2.51-2.49C28.65,12.35,28.65,7.69,27.85,3.13L27.85,3.13z M11.31,14.25V5.79l7.44,4.23
	L11.31,14.25z"/>
</svg>
