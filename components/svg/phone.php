<svg version="1.1" id="phone" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="20px" height="20px" viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
</style>
<path class="st0" d="M0,0v1.3C0,11.6,8.4,20,18.7,20H20v-6.1l-6.9-3.5l-3.7,3.7c-1.3-1-2.5-2.1-3.4-3.4L9.6,7L6.2,0H0z"/>
</svg>
