<header class="article-header entry-header">
    <?php if ((is_archive() || is_author() || is_category() || is_home() || is_search() || is_tag()) && 'post' == get_post_type()) : ?>
        <h2 class="entry-title single-title"><a href="<?php the_permalink() ?>" rel="bookmark" ><?php the_title(); ?></a></h2>
    <?php else : ?>
        <h1 class="entry-title single-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
    <?php endif;?>
    <?php // include(get_template_directory() . '/components/post/post-info-date-author.php');?>

</header> <?php // end article header?>