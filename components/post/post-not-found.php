<article id="post-not-found" class="cf">
    <section class="entry-content">
        <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'opteven'); ?></p>
    </section>
</article>