<?php wp_nav_menu(array(
    'menu' => __('Footer Bottom', 'opteven'),       // nav name
    'theme_location' => 'footer-links',             // where it's located in the theme
    'depth' => 0,                                   // limit the depth of the nav
));
