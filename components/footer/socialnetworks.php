<div class="contact">
  <a class="phone" href="tel:0476181300">
    <?php include(get_template_directory() . '/components/svg/phone.php');?>
    <span>04 76 81 13 00</span>
  </a>
  <div class="list_social">
    <a href="https://www.youtube.com/channel/UCJctf2rHLMtPnG2ZDj7mFAg" target="_blank" rel="external">
      <?php include(get_template_directory() . '/components/svg/socialnetworks/social-youtube.php');?>
    </a>
    <a href="https://www.instagram.com/branchetfrance/" target="_blank" rel="external">
      <?php include(get_template_directory() . '/components/svg/socialnetworks/social-instagram.php'); ?>
    </a>
    <a href="https://www.linkedin.com/company/cabinet-branchet/?originalSubdomain=fr" target="_blank" rel="external">
      <?php include(get_template_directory() . '/components/svg/socialnetworks/social-linkedin.php'); ?>
    </a>
    <a href="https://twitter.com/BranchetFrance" target="_blank" rel="external">
      <?php include(get_template_directory() . '/components/svg/socialnetworks/social-twitter.php'); ?>
    </a>
  </div>
</div>