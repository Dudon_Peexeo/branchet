<?php wp_nav_menu(array(
    'container' => '',                              // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    'menu' => __('Attributes', 'opteven'),          // nav name
    'theme_location' => 'footer-links',             // where it's located in the theme
    'before' => '',                                 // before the menu
    'after' => '',                                  // after the menu
    'link_before' => '',                            // before each link
    'link_after' => '',                             // after each link
    'depth' => 0,                                   // limit the depth of the nav
    'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
));
