<a class="made_by" rel="external" target="_blank" href="https://www.peexeo.com/">
    <span aria-hidden="true" focusable="false"><?php _e('Made by', 'branchet'); ?></span>
    <?php include(get_template_directory() . '/components/svg/peexeo-logo.php'); ?>
</a>