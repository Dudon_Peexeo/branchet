<?php

/**
 * Expert Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 */

// Create id attribute allowing for custom "anchor" value.

// Load values and assign defaults.
$title = get_field('expert_title') ?: 'Title';

$section1 = get_field('expert_section1_btn1') ?: 'Bouton 1';
$section1_subtitle = get_field('expert_section1_subtitle') ?: 'Sous-Titre 1';
$section1_text = get_field('expert_section1_text') ?: 'Texte 1';
$section1_btn_text = get_field('expert_section1_btn')['btn-txt'] ?: 'Bouton';
$section1_btn_url = get_field('expert_section1_btn')['btn-url'] ?: 'Url';

$section2 = get_field('expert_section2_btn1') ?: 'Bouton 2';
$section2_subtitle = get_field('expert_section2_subtitle') ?: 'Sous-Titre 2';
$section2_text = get_field('expert_section2_text') ?: 'Texte 2';
$section2_btn_text = get_field('expert_section2_btn')['btn-txt'] ?: 'Bouton';
$section2_btn_url = get_field('expert_section2_btn')['btn-url'] ?: 'Url';

?>

<section id="expert" class="<?php echo esc_attr($className) ?>">
    <h2><?=$title?></h2>
    <div class="expert-btn">
        <a id="expert-btn1" class="active"><?=$section1?></a>
        <a id="expert-btn2"><?=$section2?></a>
    </div>
    <div class="expert-section1">
        <h3><?=$section1_subtitle?></h3>
        <div class="expert-section1-text">
        <p><?=$section1_text?></p>
        </div>
        <a class="btn-blue-hover-dark-blue" href="<?=$section1_btn_url?>"><?=$section1_btn_text?></a>
    </div>
    <div class="expert-section2 hidden">
        <h3><?=$section2_subtitle?></h3>
        <div class="expert-section2-text">
            <p><?=$section2_text?></p>
        </div>
        <a class="btn-blue-hover-dark-blue" href="<?=$section2_btn_url?>"><?=$section2_btn_text?></a>
    </div>
</section>