<?php

/**
 * signature Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'signature-';
if (!empty($block['anchor'])) {
    $id .= $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if (!empty($block['className'])) {
    $className = $block['className'];
}

// Load values and assign defaults.
$text = get_field('signature') ?: 'Your signature here...';
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className) ?>">
    <p><?php echo $text; ?></p>
</div>