<div id="site_map_cat">
    <h2><?php _e('Categories', 'opteven'); ?></h2>
    <ul>
        <?php // https://developer.wordpress.org/reference/functions/wp_list_categories/?>
        <?php wp_list_categories(array(
            'show_count' 	=> true,
            'hierarchical' 	=> false,
            'title_li'  	=> '',
            'order'			=> 'ASC',
            'orderby'       => 'name',
            'style'			=> 'list',
            'show_option_none' => __('No categories'),
        )); ?>
    </ul>
</div>