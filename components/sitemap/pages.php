<div id="site_map_pages">
    <h2><?php _e('Pages', 'opteven'); ?></h2>
    <ul>
        <?php // https://developer.wordpress.org/reference/functions/wp_list_pages/?>
        <?php wp_list_pages(array(
            'title_li'  => '',
            'exclude'	=> '' //indiquer les id des pages à ne pas afficher
        )); ?>
    </ul>
</div>