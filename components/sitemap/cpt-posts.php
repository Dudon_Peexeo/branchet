
<div id="site_map_cpt_posts">
    <h2><?php _e('Tous les "custome post type" articles', 'opteven'); ?></h2>
    <?php // the query?>
    <?php $all_cpt_posts = new WP_Query(array(
        'post_type'=>'custom_type',
        'post_status'=>'publish',
        'posts_per_page'=>-1
    )); ?>
    <?php if ($all_cpt_posts->have_posts()) : ?>
        <ul>
            <!-- the loop -->
            <?php while ($all_cpt_posts->have_posts()) : $all_cpt_posts->the_post(); ?>
                <li>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    (<?php comments_number('0', '1', '%'); ?>)
                </li>
            <?php endwhile; ?>
            <!-- end of the loop -->
        </ul>
    <?php wp_reset_postdata(); endif;?>
</div>