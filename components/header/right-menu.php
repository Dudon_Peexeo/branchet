<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p>
?>
<div class="right-menu">
    <div class="right-menu-hotline">
        <a class="right-menu-hotline-image" href="tel:+33485858585">
            <img src='<?= get_template_directory_uri() . "/library/images/header/hotline-sticky.svg)" ?>'/>
            <span class="right-menu-hotline-content">UNE URGENCE ?<br>
                04 85 85 85 85 </span>
        </a>

    </div>
    <div class="right-menu-mail">
        <a class="right-menu-mail-image" href="tel:+33485858585">
            <img src='<?= get_template_directory_uri() . "/library/images/header/mail.svg)" ?>'/>
            <span class="right-menu-mail-content">Nous Contacter</span>
        </a>

    </div>
    <div class="right-menu-phone">
        <a class="right-menu-phone-image" href="tel:+33485858585">
            <img src='<?= get_template_directory_uri() . "/library/images/header/phone.svg)" ?>'/>
            <span class="right-menu-phone-content">Être Rappelé</span>
        </a>

    </div>
</div>
<?php // if you'd like to use the site description you can un-comment it below
?>
<?php // bloginfo('description');
?>