<nav id="navigation" role="navigation">
  <?php wp_nav_menu(array(
    'container' => false,                           // remove nav container
    'container_class' => 'menu cf',                 // class of container (should you choose to use it)
    'menu' => __('The Main Menu', 'branchet'),       // nav name
    'menu_class' => 'nav top-nav cf',               // adding custom nav class
    'theme_location' => 'main-nav',                 // where it's located in the theme
  )); ?>

  <?php include(get_template_directory() . '/components/header/hotline.php'); ?>
  <?php include(get_template_directory() . '/components/header/top-header-mobile.php'); ?>
</nav>


<button id="burger" type="button" aria-label="Menu" aria-controls="navigation" aria-expanded="true/false">
  <div class="burger-box">
    <div class="burger-inner"></div>
  </div>
</button>