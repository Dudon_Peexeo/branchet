<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p>
?>
<div class="hotline">
    <a href="tel:+33485858585">
        <img src='<?php echo get_template_directory_uri() . "/library/images/header/hotline-sticky.svg)" ?>' width="50" />
        
    </a>
    <div class="hotline-content">
    <a class="hotline-text" href="tel:+33485858585">
        <p>Une urgence ?</p>    
        <p>04 85 85 85 85 </p>
    </a>
    </div>
</div>
<?php // if you'd like to use the site description you can un-comment it below
?>
<?php // bloginfo('description');
?>