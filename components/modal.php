<div class="modal">
  <button class="modal-opener" onclick="openDialog('modal', this)">
    <?php _e('OUVRIR MODAL', 'opteven'); ?>
    <?php // accessibilité : texte masqué hors écran?>
    <span class="sr-only"><?php _e('Ce bouton ouvre un modal', 'opteven'); ?></span>
  </button>

  <div id="modal" role="dialog" aria-label="modal" aria-describedby="modal" aria-modal="true">
    <button class="close icon" onclick="closeDialog(this)" style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/icons/fermer.svg')"></button>
  </div>
</div>
