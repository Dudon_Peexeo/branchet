<?php get_header(); ?>

<main id="main" class="main">

    <section class="header-top">
        <div class="wrap">
            <nav aria-label="Breadcrumb" >
                <?php custom_breadcrumbs(); ?>
            </nav>
            <h1>
                <?php $title = the_title(); ?>
            </h1>		
        </div>
    </section>

    <!-- Single Job Content -->
    <section class="single-job-content">
        <div class="wrap">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <section class="entry-content">
                    <?php the_content(); ?>
                </section> 

            <?php endwhile; ?>

            <?php else : ?>

                <?php include(get_template_directory() . '/components/post/post-not-found.php'); ?>

            <?php endif; ?>
        </div>
    </section>


    <!-- Related Posts -->
    <?php include(get_template_directory() . '/include/jobs/single/related-posts.php'); ?>

    <!-- Related Testimony -->
    <?php include(get_template_directory() . '/include/jobs/single/related-testimony.php'); ?>

</main>

<?php get_footer(); ?>
