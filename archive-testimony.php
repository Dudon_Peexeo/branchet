<?php get_header(); ?>

	<div id="content">

		<section class="header-top">
			<div class="wrap">
				<nav aria-label="Breadcrumb" >
					<ul id="breadcrumbs" class="breadcrumbs">
						<li class="item-home"><a class="bread-home"  href="/"><?php _e('Accueil', 'opteven') ?></a></li>
						<li class="item-parent"><a class="bread-parent" href="/nos-metiers"><?php _e('Nos métiers', 'opteven');?></a></li>
						<li aria-current="page" class="item-current"><strong><?php post_type_archive_title(); ?></strong></li>
					</ul>
				</nav>
				<h1>
					<?php post_type_archive_title(); ?>
				</h1>		
			</div>
		</section>

		<main id="main" role="main">	

			<!-- Videos -->
			<?php include(get_template_directory() . '/include/testimony/videos.php'); ?>

			<!-- Interviews -->
			<?php include(get_template_directory() . '/include/testimony/interviews.php'); ?>

			<!-- Slider -->
			<?php include(get_template_directory() . '/include/testimony/slider.php'); ?>

			<!-- Candidate -->
			<?php include(get_template_directory() . '/include/testimony/candidate.php'); ?>

		</main>
	</div>

<?php get_footer(); ?>
