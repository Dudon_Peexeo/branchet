			<footer class="footer">

				<div class="footer-top">
					<div class="footer-container wrap">
						<a href="<?= home_url(); ?>"><img class="footer-logo" src='<?= get_template_directory_uri() . "/library/images/icons/logo-branchet-white.svg" ?>' /></a>
						<?php include(get_template_directory() . '/components/footer/footer-top.php'); ?>
						<?php include(get_template_directory() . '/components/footer/footer-bottom.php'); ?>
						<?php include(get_template_directory() . '/components/footer/socialnetworks.php'); ?>		
					</div>		
				</div>

				<div class="footer-bottom">
					<div class="footer-container wrap">
						<?php include(get_template_directory() . '/components/footer/copyright.php'); ?>			
						<?php include(get_template_directory() . '/components/footer/attributes.php'); ?>
						<?php include(get_template_directory() . '/components/footer/madeby.php'); ?>
					</div>
				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php?>
		<?php wp_footer(); ?>
	</body>
</html> <!-- end of site. what a ride! -->
