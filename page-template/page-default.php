<?php
/*
 Template Name: Default
*/
?>

<?php get_header(); ?>

<main id="main">

    <section class="header-top" style="background-image:url('<?php the_post_thumbnail_url('full') ?>')">
        <div class="wrap">
            <nav aria-label="Breadcrumb" >
                <?php custom_breadcrumbs(); ?>
            </nav>
            <h1>
                <?php the_title(); ?>
            </h1>		
        </div>
    </section>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="wrap entry-content">

            <?php the_content() ?>

        </div>

	<?php endwhile; endif; ?>
</main>

<?php get_footer(); ?>
