<?php
/*
 Template Name: Contact
*/
?>

<?php get_header(); ?>

<main id="main">

    <!-- Top -->
    <section class="header-top" style="background-image:url('<?php the_post_thumbnail_url('full') ?>')">
        <div class="wrap">
            <nav aria-label="Breadcrumb" >
                <?php custom_breadcrumbs(); ?>
            </nav>
            <h1>
                <?php the_title(); ?>
            </h1>		
        </div>
    </section>

    <?php include(get_template_directory() . '/include/page/contact/map.php'); ?>

    <?php include(get_template_directory() . '/include/page/contact/form.php'); ?>
  
</main>

<?php get_footer(); ?>
