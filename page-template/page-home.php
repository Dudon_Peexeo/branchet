<?php
/*
 Template Name: Home
*/
?>

<?php get_header(); ?>

<main id="main" class="page-home" role="main">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<!-- Header -->
			<?php include(get_template_directory() . '/include/home/header.php'); ?>

			<!-- Offers -->
			<?php include(get_template_directory() . '/include/general/offers.php'); ?>

			<!-- Benefit -->
			<?php include(get_template_directory() . '/include/home/benefit.php'); ?>

			<!-- Call To Action -->
			<?php include(get_template_directory() . '/include/home/call-to-action.php'); ?>

			<!-- Speciality -->
			<?php include(get_template_directory() . '/include/home/speciality.php'); ?>

			<!-- Posts -->
			<?php include(get_template_directory() . '/include/home/blog.php'); ?>

			<!-- Informations -->
			<?php include(get_template_directory() . '/include/home/infos.php'); ?>

			<!-- Partner -->
			<?php include(get_template_directory() . '/include/home/partner.php'); ?>

			<!-- Testimony -->
			<?php include(get_template_directory() . '/include/general/testimony.php'); ?>

	<?php endwhile;
    endif; ?>
</main>

<?php get_footer(); ?>