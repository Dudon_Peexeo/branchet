<?php
/*
 Template Name: Thank you
*/
?>

<?php get_header(); ?>

<main id="main">

    <!-- Top -->
    <section class="header-top" style="background-image:url('<?php the_post_thumbnail_url('full') ?>')">
        <div class="wrap">
            <nav aria-label="Breadcrumb" >
                <?php custom_breadcrumbs(); ?>
            </nav>
            <h1>
                <?php the_title(); ?>
            </h1>		
        </div>
    </section>

    <?php
    $thanks_title = get_field('thank_you_title');
    $thanks_txt   = get_field('thank_you_txt');
    $thanks_link  = get_field('thank_you_link');
    ?>

    <section class="thank-you">
        <div class="wrap">
            <h2><?= $thanks_title ?></h2>
            <?= $thanks_txt ?>
            <a class="button-link" href="<?= $thanks_link['url'] ?>"><?= $thanks_link['txt'] ?></a>
        </div>
    </section>

</main>

<?php get_footer(); ?>
