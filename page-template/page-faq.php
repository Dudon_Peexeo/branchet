<?php
/*
 Template Name: FAQ
*/
?>

<?php get_header(); ?>

<main id="main">

    <section class="header-top" style="background-image:url('<?php the_post_thumbnail_url('full') ?>')">
        <div class="wrap">
            <nav aria-label="Breadcrumb" >
                <?php custom_breadcrumbs(); ?>
            </nav>
            <h1>
                <?php the_title(); ?>
            </h1>		
        </div>
    </section>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
		<!-- Candidate -->
        <?php include(get_template_directory() . '/include/page/faq/candidate.php'); ?>

        <!-- Join us -->
        <?php include(get_template_directory() . '/include/page/faq/join-us.php'); ?>

        <!-- Jobs -->
        <?php include(get_template_directory() . '/include/page/faq/jobs.php'); ?>

        <!-- Application -->
        <?php include(get_template_directory() . '/include/page/faq/application.php'); ?>

	<?php endwhile; endif; ?>
</main>

<?php get_footer(); ?>
