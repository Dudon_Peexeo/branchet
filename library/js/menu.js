// Animations du menu burger sur mobile + accessibilité du sous menu sur desktop

jQuery(document).ready(function($) {
    // Menu burger déroulant
    $('#burger').click(function() {
        $(this).toggleClass('is-active');
        $('#inner-header nav').toggleClass('show-menu');
    });

    // Menu burger : sous-menu déroulant
    $('#navigation >ul >li.menu-item-has-children').click(function() {
        if ($(window).width() < 1030) {
            $(this).children('.sub-menu').slideToggle();
            $(this).toggleClass('dropped-down');
        }
    });

    // from Menu mobile to Menu Desktop : dynamiquement au passage de 1030px
    $(window).resize(function() {
        if ($(window).width() > 1030) {
            $('#navigation').removeClass('show-menu');
            $('#burger').removeClass('is-active');
        }
    });

    
});