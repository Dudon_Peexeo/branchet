// Remove Button Block Styles
// source: https://www.billerickson.net/wordpress-color-palette-button-styling-gutenberg/
// the options may have changed

// button
wp.domReady(() => {
  wp.blocks.unregisterBlockStyle('core/button', 'default');
  wp.blocks.unregisterBlockStyle('core/button', 'outline');
  wp.blocks.unregisterBlockStyle('core/button', 'squared');
  wp.blocks.unregisterBlockStyle('core/button', 'fill');
});

// quote
wp.domReady(() => {
  wp.blocks.unregisterBlockStyle('core/quote', 'default');
  wp.blocks.unregisterBlockStyle('core/quote', 'large');
});

// table
wp.domReady(() => {
  wp.blocks.unregisterBlockStyle('core/table', 'regular');
  wp.blocks.unregisterBlockStyle('core/table', 'stripes');
});

// image
wp.domReady(() => {
  wp.blocks.unregisterBlockStyle('core/image', 'default');
  wp.blocks.unregisterBlockStyle('core/image', 'circle-mask');
});



