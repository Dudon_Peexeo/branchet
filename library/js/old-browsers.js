// message for old browsers
// http://browser-update.org/customize.html

var $buoop = {
	vs:{ i:10, f:40, o:30, s:8, c:50 },
	api:4,
	test: false,
	text: "La version de votre navigateur ({brow_name}) est trop ancienne. Pour profiter d’une meilleure expérience sur notre site, nous vous conseillons de mettre à jour votre navigateur ! <a{up_but}>Mettre à jour</a> ou <a{ignore_but}>Ignorer</a>"
};


function $buo_f() {
	var e = document.createElement("script");
	e.src = "//browser-update.org/update.min.js";
	document.body.appendChild(e);
};

try { document.addEventListener("DOMContentLoaded", $buo_f, false) }

catch(e) { window.attachEvent("onload", $buo_f) }
