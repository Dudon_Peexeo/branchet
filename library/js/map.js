function initMap() {
    // The location of opteven
    var opteven = { lat: 45.7615749, lng: 4.9192849 };
    // The map, centered at opteven
    var map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 13,
            center: opteven,
            disableDefaultUI: true
        });
    // The marker, positioned at opteven
    var image = '../../wp-content/themes/opteven/library/images/icons/icon_pinMap.svg';

    var service = new google.maps.places.PlacesService(map);
    service.getDetails({
        placeId: 'ChIJwTVZU7PB9EcR4ZVFH-vqiwI'
    }, function (result, status) {
        var marker = new google.maps.Marker({
            position: result.geometry.location,
            map: map,
            icon: image,
            place: {
                placeId: 'ChIJwTVZU7PB9EcR4ZVFH-vqiwI',
                location: result.geometry.location
            }
        });
    });

}