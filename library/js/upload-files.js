// Custom upload files in forms (with Gravity Forms)

jQuery(document).ready(function ($) {
	var inputs = $('.file-upload input[type="file"]');

	// on affiche le label (soit label d'origine, soit le nom de fichier chargé) à l'intérieur du bouton
	inputs.each(function () {
		var label = $(this).parents('.file-upload').children('label');
		var labelVal = label.text();

		$(this).change(function () {
			var fileName = $(this).val().split("\\").pop();
			if (fileName) {
				label.html(fileName);
			} else {
				label.html(labelVal);
			}
		});
	});

	$(document).on('gform_post_render', function () {
		var inputs = $('.file-upload input[type="file"]');

		// on affiche le label (soit label d'origine, soit le nom de fichier chargé) à l'intérieur du bouton
		inputs.each(function () {
			var label = $(this).parents('.file-upload').children('label');
			label.attr("tabindex", "0");
			var labelVal = label.text();

			$(this).change(function () {
				var fileName = $(this).val().split("\\").pop();
				if (fileName) {
					label.html(fileName);
				} else {
					label.html(labelVal);
				}
			});
		});
	});

});