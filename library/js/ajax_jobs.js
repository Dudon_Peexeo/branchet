(function ($) {
    $(document).ready(function () {
		
		// FILTER CONTRACT
        $.ajax({
			url: wpAjax.ajaxUrl,
			data: { 
                action: 'filter',
            }, // form data
			type: 'post', // POST
			success:function(data){
				$('#contract').append(data);
			}
		}).done(function() {

		});

		// FILTER POSTE
		$.ajax({
			url: wpAjax.ajaxUrl,
			data: { 
                action: 'filter_poste',
            }, // form data
			type: 'post', // POST
			success:function(data){
				console.log(data);
				$('#poste').append(data);
			}
		}).done(function() {

		});

		// FILTER LOCATION
		$.ajax({
			url: wpAjax.ajaxUrl,
			data: { 
                action: 'filter_location',
            }, // form data
			type: 'post', // POST
			success:function(data){
				$('#location').append(data);
			}
		}).done(function() {

		});
  
    });
})(jQuery);