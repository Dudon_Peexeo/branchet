<?php
    // Set the query for one interview

	$filterPoste = $_POST['poste'];
	$filterContract = $_POST['contract'];
	$filterLocation = $_POST['location'];
	if (!empty($filterPoste) || !empty($filterContract) || !empty($filterLocation)) {
		$args = array(
			'post_type'      => 'offers',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC',
			'meta_query'	=> array(
                'relation'		=> 'AND',
                array(
                    'key'	 	=> 'vac_town',
                    'value'	  	=> $filterLocation,
                    'compare' 	=> 'like',
                ),
                array(
                    'key'	  	=> 'vac_contract',
                    'value'	  	=> $filterContract,
                    'compare' 	=> 'like',
                ),
                array(
                    'key'	  	=> 'vacancy_title',
                    'value'	  	=> $filterPoste,
                    'compare' 	=> 'like',
                )
            ),
		);
	} else {
		$args = array(
			'post_type'      => 'offers',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
		);
	}
    $query_offers = new WP_Query($args);
    
    // Data for the section
    $object           = get_queried_object();
    $offers_title = get_field('offers-title', $object->name);
?>

<?php get_header(); ?>

	<div id="content">
		<section class="header-top">
			<div class="wrap">
				<nav aria-label="Breadcrumb" >
					<ul id="breadcrumbs" class="breadcrumbs">
						<li class="item-home"><a class="bread-home"  href="/"><?php _e('Accueil', 'opteven') ?></a></li>
						<li class="item-parent"><a class="bread-parent"  href="#"><?php _e('Offres d\'emploi', 'opteven');?></a></li>
						<li aria-current="page" class="item-current"><strong><?php post_type_archive_title(); ?></strong></li>
					</ul>
				</nav>
				<h1>
					<?php post_type_archive_title(); ?>
				</h1>		
			</div>
		</section>

		<main id="main" role="main">
			<div class="wrap">	
				<h2><?= $offers_title ?></h2>

				<!-- Display Offers filter  -->
				<div class="filter-offers">
					<form method='post' action="<?php get_template_directory_uri(); ?>/nos-offres/" >
						<select name="poste" id="poste">
							<option value=""><?php _e("Poste", "opteven") ?></option>
						</select>
						<select name="contract" id="contract">
							<option value=""><?php _e("Type de contrat", "opteven") ?></option>
						</select>
						<select name="location" id="location">
							<option value=""><?php _e("Localisation", "opteven") ?></option>
						</select>
						<button type="submit" class="button-search"><?php _e("Rechercher", "opteven") ?></button>
					</form>
				</div>

				<!-- Display offers content  -->
				<div class="wrap-content-offers">
					<?php if ($query_offers->have_posts()) : while ($query_offers->have_posts()) : $query_offers->the_post();?>
						<a href="<?php the_permalink(); ?>" class="single-offer">
							<span class="single-offer-content">
								<span class="contract"><?php the_field('vac_contract') ?></span>
								<h3><?= the_title(); ?></h3>
								<span class="grp">
									<span class="town"><?php the_field('vac_town') ?></span>
									<span class="date"><?php the_field('vacancy_activation_date') ?></span>
								</span>
							</span>
							<span class="button-link-small"><?php _e('Consulter', 'opteven') ?></span>
						</a>
					<?php endwhile; endif; wp_reset_postdata();?>
					<?php if (have_rows('home_offers')): ?>
						<?php while (have_rows('home_offers')): the_row(); ?>
							<?php $link = get_sub_field('link'); ?>
							<a href="<?= $link['url'] ?>" class="single-offer last">
								<span class="button-link"><?= $link['txt'];?></span>
							</a>
						<?php endwhile; ?>
					<?php endif; ?>
					
				</div>

			</div>

		</main>
	</div>

<?php get_footer(); ?>
