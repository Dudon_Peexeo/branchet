<section class="offers">
    <div class="wrap">
        <h2 class="main-title"><?= get_field('home_offers_title'); ?></h2>
        <div class="offers-container">

            <?php if (have_rows('home_offers')) : ?>
                <?php while (have_rows('home_offers')) : the_row(); ?>
                    <?php
                    // Data for the loop
                    $title = get_sub_field('title');
                    $img   = get_sub_field('img');
                    ?>
                    <div class="offer-box">
                         <h4><?= $title ?></h4>
                        <div class="offer-recto">
                            <img src="<?= $img ?>" alt="">
                        </div>
                        <div class="offer-verso">

                            <?php if (have_rows('description')) : ?>
                                <?php while (have_rows('description')) : the_row(); ?>
                                    <?php
                                    // Data for the loop
                                    $txt  = get_sub_field('content');
                                    $link = get_sub_field('link');
                                    ?>
                                    <?= $txt ?>
                                    <a class="btn-blue-hover-dark-blue" href="<?= $link['url'] ?>"><?= $link['txt'] ?></a>
                            <?php endwhile;
                            endif; ?>

                        </div>
                    </div>                    
            <?php endwhile;
            endif; ?>

        </div>
    </div>
</section>