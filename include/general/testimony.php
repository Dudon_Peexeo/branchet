<?php
    // Testimony loop
    $args = array(
        'post_type'      => 'testimony',
        'orderby'        => 'date',
        'order'          => 'DSC',
        'posts_per_page' => -1
    );
    
    $blog = new WP_Query($args);
?>

<section class="testimony">
    <div class="wrap">
        <h2 class="main-title"><?= _e('Ce qu\'ils disent de nous', 'Branchet') ?></h2>

        <?php if ($blog->have_posts()) : ?>   

            <ul id="testimony-slides slider" class="testimony-slides">
    
                <?php while ($blog->have_posts()) : $blog->the_post(); ?>
                    <?php
                        // Get the post title
                        $title = get_the_title();
                        // Get the post first lines
                        $content = wp_trim_words(get_the_content(), 60);
                        // Get the category
                        $category = get_the_term_list(get_the_ID(), 'custom_cat_testimony');
                    ?>

                    <li class="testimony-slide">
                        <p><?= $content ?></p>
                        <div class="author">
                            <h4><?= $title ?></h4> - <span><?= $category ?></span>
                        </div>
                    </li>
                <?php endwhile; ?>

            </ul>

            <div class="dots-testimony"></div>

            <div class="container">
                <a class="btn-blue-medium-hover-dark" href="/temoignages"><?= _e('Tous les témoignages', 'Branchet') ?></a>
            </div>
            
        <?php endif; wp_reset_query() ?>
    </div>
</section>


