<?php
    // Set the query for one interview
    $args = array(
        'post_type'      => 'interviews',
        'post_status'    => 'publish',
        'posts_per_page' => 1,
        'tax_query'      => array(
            array(
                'taxonomy' => 'custom_cat_interview',
                'field'    => 'slug',
                'terms'    => array( 'interview-a-la-une' ),
            ),
        ),
    );
    $query_interview = new WP_Query($args);

    // Data for the section
    $object = get_queried_object();

    $interview_title = get_field('interview-title', $object->name);
    $interview_link  = get_field('interview-link', $object->name);
?>

<section class="interview">
    <div class="wrap">
        <div class="interview-content">
            <div class="title-interview">
                <h2><?= $interview_title ?></h2>
                <a href="<?= $interview_link['url'] ?>" class="button-link"><?= $interview_link['txt'] ?></a>
            </div>

            <!-- Start the loop for one video testimony -->
            <?php if ($query_interview->have_posts()) : while ($query_interview->have_posts()) : $query_interview->the_post();?>

            <div class="author-identity">
                <!-- Display the interview author's image -->
                <img src="<?= the_post_thumbnail_url('full') ?>" alt="">

                <!-- Display the interview author's name -->
                <div class="wp-signature">
                    <p><?php the_title() ?></p>
                </div>
                
                <!-- Display the interview author's job -->
                <?php $job = get_field('relation_jobs_interview'); ?>
                <?php foreach ($job as $j): ?>
                    <h5><?= $j->post_title ?></h5>
                <?php endforeach; ?>
            </div>

            <!-- Display the slider interview -->
            <div class="slider-container">
                <div class="slider-interview">
                    <?php if (have_rows('interview')): ?>
                        <ul class="slides-interview">
                            <?php while (have_rows('interview')): the_row(); ?>
                                <li class="slide">								
                                    <h3><?= get_sub_field('question') ?></h3>
                                    <p><?= get_sub_field('response') ?></p>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>		
            </div>			

            <?php endwhile; endif;?><!-- End the loop for one testimony -->					
        </div>
    </div>
</section>