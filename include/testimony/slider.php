<!-- Set the query testimony  -->
<?php
    $args = array(
        'post_type'      => 'testimony',
        'post_status'    => 'publish',
        'posts_per_page' => -1,
        'orderby'        => 'menu_order',
        'order'          => 'DESC'
    );
    $query_testimony = new WP_Query($args);
?>

<section class="slider-testimony">
    <div class="wrap">
        <ul class="slides-archive-testimony">
            <?php while ($query_testimony->have_posts()) : $query_testimony->the_post();?>
                <li class="slide">
                    <div class="img-container">
                        <img src="<?php the_post_thumbnail_url('full');?>" alt="">
                    </div>
                    <div class="the-content">

                        <!-- Display the testimony -->
                        <?php the_content() ?>

                        <!-- Display the testimony author -->
                        <div class="wp-signature">
                            <p><?php the_title() ?></p>
                        </div>

                        <!-- Display the testimony author's job -->
                        <?php $post = get_field('relation_jobs_testimony'); ?>
                        <div class="jobs">
                            <?php foreach ($post as $p): ?>
                                <h5><?= $p->post_title ?></h5>
                            <?php endforeach; ?>
                        </div>    
                    </div>
                </li>            
            <?php endwhile; ?>
        </ul>
    </div>
</section>