<?php
    // Data for the section
    $object = get_queried_object();

    $candidate_title = get_field('candidate-title', $object->name);
    $candidate_types = get_field('candidate-type', $object->name);
?>

<section class="application">
    <div class="wrap">
        <h2><?= $candidate_title ?></h2>
        <div class="application-container">

            <!-- Display all types of application  -->
            <?php if ($candidate_types) : foreach ($candidate_types as $candidate_type) : ?>
                <div class="box">
                    <h3><?= $candidate_type['title'] ?></h3>
                    <img src="<?= $candidate_type['icon'] ?>" width="110" height="110" alt="">
                    <a href="<?= $candidate_type['link']['url'] ?>" class="button-link"><?= $candidate_type['link']['txt'] ?></a>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>