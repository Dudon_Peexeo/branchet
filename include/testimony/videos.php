<?php
    // Set the query for one video testimony
    $args = array(
        'post_type'      => 'videos',
        'post_status'    => 'publish',
        'posts_per_page' => 1,
        'tax_query'      => array(
            array(
                'taxonomy' => 'custom_cat_videos',
                'field'    => 'slug',
                'terms'    => array( 'video-a-la-une' ),
            ),
        ),
    );
    $query_video = new WP_Query($args);

    // Set the query for all videos testimonies
    $args = array(
        'post_type'      => 'videos',
        'post_status'    => 'publish',
        'posts_per_page' => 6,
        'order'          => 'ASC'
    );
    $query_videos = new WP_Query($args);

    // Data for the section
    $object        = get_queried_object();

    $testimony_title    = get_field('testimony-title', $object->name);
    $testimony_subtitle = get_field('testimony-subtitle', $object->name);

?>
            
<section class="videos-testimony">
    <div class="wrap">

        <h2><?= $testimony_title ?></h2>
        <h3><?= $testimony_subtitle ?></h3>

        <div class="videos-content">

            <div class="video-selected">
                <!-- Start the loop for one video testimony -->
                <?php if ($query_video->have_posts()) : while ($query_video->have_posts()) : $query_video->the_post();?>

                <!-- Display the image for the video testimony -->
                <div class="image-container">
                    <img src="<?= get_the_post_thumbnail_url($post, 'full') ?>" alt="">
                    <img src="<?= get_template_directory_uri() . '/library/images/icons/play.svg' ?>">
                </div>

                <!-- Display the testimony author's video -->
                <iframe src="<?= get_field('video') ?>" frameborder="0" allowfullscreen></iframe>				
                <h3>	
                    <!-- Display the testimony author's name -->						
                    <span class="single-video-title"><?php the_title() ?></span>,					

                    <!-- Display the testimony author's job -->
                    <?php $post = get_field('relation_jobs_videos'); ?>
                    <?php foreach ($post as $p): ?>
                        <span class="single-video-job"><?= $p->post_title ?></span>
                    <?php endforeach; ?>
                </h3>

                <!-- Display the testimony author's description -->		
                <span class="single-video-description"><?php the_content() ?></span>

                <?php endwhile; endif;?><!-- End the loop for one testimony -->
            </div>

        <div class="videos">

            <!-- Start the loop for all videos testimonies -->
            <?php if ($query_videos->have_posts()) : while ($query_videos->have_posts()) : $query_videos->the_post();?>
                <div class="single-video">
                    <?php $video = get_field('video'); ?>

                    <!-- Display the testimony author's image -->
                    <div class="image-container">
                        <img src="<?= get_the_post_thumbnail_url($post, 'full') ?>" alt="">
                        <img src="<?= get_template_directory_uri() . '/library/images/icons/play.svg' ?>">
                    </div>

                    <p class="single-video-content">	

                        <!-- Display the testimony author's name -->														
                        <span class="single-video-title"><?php the_title() ?></span>,	

                        <!-- Display the testimony author's job -->
                        <?php $post = get_field('relation_jobs_videos'); ?>
                        <?php foreach ($post as $p): ?>
                            <?php $job_title = $p->post_title; ?>
                            <span class="single-video-job"><?= $job_title ?></span>
                        <?php endforeach; ?>
                    </p>
                    <span class="single-video-description"><?php the_content() ?></span>
                    <iframe src="<?= $video ?>" frameborder="0" allowfullscreen></iframe>							
                </div>
            <?php endwhile; endif;?><!-- End the loop for all testimonies -->
        </div>
    </div>
</section>