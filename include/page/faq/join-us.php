<?php
// Data for section
$plus  = get_template_directory_uri() . '/library/images/icons/plus.svg';
$minus = get_template_directory_uri() . '/library/images/icons/moins.svg';
?>

<section class="faq">
    <div class="wrap">

        <h2><?= get_field('section_join_us_title') ?></h2>

        <?php if (have_rows('faq_join_us')) :
            while (have_rows('faq_join_us')) : the_row(); ?>
            <?php
            // Data for the loop
            $question = get_sub_field('faq_join_us_question');
            $response = get_sub_field('faq_join_us_response');
            ?>

            <div class="faq-container">
                <div class="head-container">
                    <h3><?= $question ?></h3>
                    <hr>
                    <div class="button-container">
                        <img class="plus" src="<?= $plus ?>" alt="">
                        <img class="minus invisible" src="<?= $minus ?>" alt="">
                    </div>
                </div>
                <div class="txt-container invisible">
                    <p><?= $response ?></p>
                </div>                
            </div>
        <?php endwhile; endif; ?>
    </div>
</section>