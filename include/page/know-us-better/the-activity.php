<?php
// Data for section

$title    = get_field('know_us_better_activity_title');
$img_top  = get_field('know_us_better_img_bottom');
$txt_top  = get_field('know_us_better_txt_bottom');
?>

<section class="the-activity">
    <div class="wrap">
        <div class="container">
            <h2><?= $title ?></h2>
            <?= $txt_top ?>
        </div>
        <img src="<?= $img_top ?>" alt="">
    </div>
</section>
