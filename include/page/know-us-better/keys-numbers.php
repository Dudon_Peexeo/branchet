<section class="key-numbers">
    <ul class="wrap">
    <?php
    if (have_rows('know_us_better_keys_numbers')):
        $count = 0;
        while (have_rows('know_us_better_keys_numbers')) : the_row();
    ?>
        <li>
            <div class="number">
                <span id="number_<?= $count; ?>">
                    <?php the_sub_field('number');?>
                </span>
                <span>
                    <?php the_sub_field('unite');?>
                </span>
            </div>
            <p class="txt"><?php the_sub_field('txt');?></p>
        </li>
    <?php
            $count++; endwhile;
    else :
    endif;
    ?>
    </ul>
</section>