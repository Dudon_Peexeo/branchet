<?php
// Data for section
$title = get_field('know_us_better_numbers_title');
?>

<section class="few-numbers">
    <div class="wrap">
        <h2><?= $title ?></h2>

        <div class="global-container">

        <?php if (have_rows('know_us_better_few_numbers')): ?>
            <?php while (have_rows('know_us_better_few_numbers')): the_row(); ?>
                <div class="box-container">
                    <h3><?= get_sub_field('subtitle') ?></h3>

                    <?php $the_numbers = get_sub_field('the-numbers'); ?>

                    <div class="numbers-container">
                        <?php foreach ($the_numbers as $the_number) : ?>
                        <div class="description-container">
                            <h2><?= $the_number['unite'] ?></h2>
                            <h3><?= $the_number['txt'] ?></h3>
                        </div>
                        <?php endforeach ?> 
                    </div>

                </div>
            <?php endwhile; ?>                        
        <?php endif; ?>
        </div>
    </div>
</section>
