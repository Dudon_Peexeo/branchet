<?php
// Data for section

$title    = get_field('know_us_better_title');
$subtitle = get_field('know_us_better_subtitle');
$img_top  = get_field('know_us_better_img_top');
$txt_top  = get_field('know_us_better_txt_top');
?>

<section class="introduction">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <h3><?= $subtitle ?></h3>
        <div class="img-txt">
            <img src="<?= $img_top ?>" alt="">
            <div class="container">
                <?= $txt_top ?>
            </div>
        </div>
    </div>
</section>
