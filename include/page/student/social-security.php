<?php
// Data for section
$txt = get_field('student_social_security_txt');
?>

<section class="social-security">
    <div class="wrap">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
        </div>
    </div>
</section>