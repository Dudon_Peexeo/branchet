<?php
// Data for section
$img    = get_field('student_how_to_img');
$txt    = get_field('student_how_to_txt');
?>

<section class="how-to">
    <div class="wrap">
        <div class="how-to-container">
            <img src="<?= $img ?>" alt="">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
        </div>
    </div>
</section>