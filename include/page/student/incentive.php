<?php
// Data for section
$txt = get_field('student_incentive_txt');
?>

<section class="incentive">
    <div class="wrap">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
        </div>
    </div>
</section>