<?php
// Data for section
$title  = get_field('student_internship_title');
$img    = get_field('student_internship_img');
$txt    = get_field('student_internship_txt');
$button = get_field('student_internship_link');
?>

<section class="internship">
    <div class="wrap">
        <div class="internship-container">
            <div class="txt-container">
                <h2><?= $title ?></h2>
                <div class="entry-content">
                    <p><?= $txt ?></p>
                </div>
                <a class="button-link" href="<?= $button['url'] ?>"><?= $button['txt'] ?></a>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
    </div>
</section>