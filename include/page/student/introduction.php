<?php
// Data for section
$txt         = get_field('student_introduction_txt');
$catchphrase = get_field('student_introduction_catchphrase');
$img         = get_field('student_introduction_img');
?>

<section class="introduction">
    <div class="wrap">
        <div class="introduction-container">
            <div class="txt">
                <p><?= $txt ?></p>
                <div class="catchphrase-container">
                    <h3><?= $catchphrase ?></h3>
                </div>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
    </div>
</section>