<?php
// Data for section
$title = get_field('student_application_title');
?>

<section class="application">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="application-container">

            <!-- Display all types of application  -->
            <?php if (have_rows('student_application_box')) : ?>
                <?php while (have_rows('student_application_box')) : the_row(); ?>

            <div class="box">
                <?php
                // Data for the loop
                $box_title = get_sub_field('student_application_box_title');
                $box_img   = get_sub_field('student_application_box_img');
                $box_link  = get_sub_field('student_application_box_link');

                ?>
                <h3><?= $box_title ?></h3>
                <img src="<?= $box_img ?>" alt="">
                <a href="<?= $box_link['url'] ?>" class="button-link"><?= $box_link['txt'] ?></a>
            </div>

            <?php endwhile; endif;?>
        </div>
    </div>
</section>