<?php
// Data for section
$title    = get_field('student_vacation_title');
$subtitle = get_field('student_vacation_subtitle');
$img      = get_field('student_vacation_img');
$txt      = get_field('student_vacation_txt');
?>

<section class="vacation">
    <div class="wrap">
        <h3><?= $title ?></h3>
        <p><?= $subtitle ?></p>
        <div class="vacation-container">
            <img src="<?= $img ?>" alt="">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
        </div>
    </div>
</section>