<?php
// Data for section
$title       = get_field('student_contracts_title');
$subtitle    = get_field('student_contracts_subtitle');
$col_1       = get_field('student_contracts_col_1');
$col_2       = get_field('student_contracts_col_2');
$catchphrase = get_field('student_contracts_catchphrase')
?>

<section class="contracts">
    <div class="wrap">
        <h3><?= $title ?></h3>
        <p><?= $subtitle ?></p>
        <div class="contracts-container">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th><h3><?= $col_1 ?></h3></th>
                        <th><h3><?= $col_2 ?></h3></th>
                    </tr>
                </thead>
                <tbody>              
                    <?php if (have_rows('student_contracts_rows')) : ?>
                        <?php while (have_rows('student_contracts_rows')) : the_row(); ?>

                        <tr>
                            <td><?= get_sub_field('column_0'); ?></td>
                            <td><?= get_sub_field('column_1'); ?></td>
                            <td><?= get_sub_field('column_2'); ?></td>
                        </tr>

                    <?php endwhile; endif;?>
                </tbody>
            </table>
            <div class="catchphrase-container">
                <h3><?= $catchphrase ?></h3>
            </div>
        </div>
    </div>
</section>