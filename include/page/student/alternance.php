<?php
// Data for section
$title  = get_field('student_alternance_title');
$img    = get_field('student_alternance_img');
$txt    = get_field('student_alternance_txt');
$button = get_field('student_alternance_link');
?>

<section class="alternance">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="alternance-container">
            <img src="<?= $img ?>" alt="">

            <div class="txt-container">
                <div class="entry-content">
                    <p><?= $txt ?></p>
                </div>
                <a class="button-link" href="<?= $button['url'] ?>"><?= $button['txt'] ?></a>
            </div>
        </div>
    </div>
</section>