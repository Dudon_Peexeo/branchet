<?php
// Data for section
$txt = get_field('student_remuneration_txt');
$img = get_field('student_remuneration_img');
?>

<section class="remuneration">
    <div class="wrap">
        <div class="remuneration-container">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
    </div>
</section>