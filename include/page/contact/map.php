<?php
    //GOOGLE MAPS
    wp_register_script('mapInit', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBHbBBoHVEhV2y9XYaAS0Cu10G8MI79c5c&libraries=places&callback=initMap', array(), '', true);
    wp_register_script('map', get_stylesheet_directory_uri() . '/library/js/map.js', array(), '', true);
    wp_enqueue_script('mapInit');
    wp_enqueue_script('map');


function add_async_attribute($tag, $handle)
{
    if ('mapInit' !== $handle) {
        return $tag;
    }
    return str_replace(' src', ' async="async" src', $tag);
}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

function add_defer_attribute($tag, $handle)
{
    if ('mapInit' !== $handle) {
        return $tag;
    }
    return str_replace(' src', ' defer="defer" src', $tag);
}

add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
