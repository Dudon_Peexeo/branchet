<?php
// Data for section
$section_title   = get_field('contact_title');
$find_us_title   = get_field('contact_find_us_subtitle');
$find_us_address = get_field('contact_find_us_address');
$questions_title = get_field('contact_questions_subtitle');
$questions_txt   = get_field('contact_questions_txt');
$questions_link  = get_field('contact_questions_link');
?>

<section class="contact">
    <div class="wrap">
        <div class="the-content">
            <div class="infos">
                <div id="map"></div>

                <div class="txt-container">
                    <div class="find-us">
                        <h3><?= $find_us_title ?></h3>
                        <p><?= $find_us_address ?></p>
                    </div>

                    <div class="questions">
                        <h3><?= $questions_title ?></h3>
                        <p><?= $questions_txt ?></p>
                        <a class="button-link-small-blue" href="<?= $questions_link['url'] ?>"><?= $questions_link['txt'] ?></a>
                    </div>
                </div>
            </div>

            <div class="form">
                <h2><?= $section_title ?></h2>
                <?php gravity_form(1, false, false, false, '', true, 0); ?>
            </div>
        </div>
    </div>
</section>   