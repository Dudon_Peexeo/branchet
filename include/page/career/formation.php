<?php
// Data for section
$title    = get_field('career_formation_title');
$img      = get_field('career_formation_img');
$txt      = get_field('career_formation_txt');
?>

<section class="formation">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="formation-container">
            <img src="<?= $img ?>" alt="">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
        </div>
    </div>
</section>