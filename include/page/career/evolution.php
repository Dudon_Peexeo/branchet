<?php
// Data for section
$title    = get_field('career_evolution_title');
$img      = get_field('career_evolution_img');
$txt      = get_field('career_evolution_txt');
?>

<section class="evolution">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="evolution-container">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
    </div>
</section>