<?php
// Data for section
$title    = get_field('career_remuneration_title');
$subtitle = get_field('career_remuneration_subtitle');
$phrase   = get_field('career_remuneration_phrase');
$img      = get_field('career_remuneration_img');
$txt      = get_field('career_remuneration_txt');
?>

<section class="remuneration">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <h3><?= $subtitle ?></h3>
        <p><?= $phrase ?></p>
        <div class="remuneration-container">
            <img src="<?= $img ?>" alt="">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
        </div>
    </div>
</section>