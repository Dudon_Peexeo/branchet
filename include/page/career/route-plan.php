<?php
// Data for section
$title    = get_field('career_route_plan_title');
$img      = get_field('career_route_plan_img');
$txt      = get_field('career_route_plan_txt');
?>

<section class="route-plan">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="route-plan-container">
            <img src="<?= $img ?>" alt="">
            <div class="entry-content">
                <p><?= $txt ?></p>
            </div>
        </div>
    </div>
</section>