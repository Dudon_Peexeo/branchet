<?php
// Data for section
$title       = get_field('career_integration_title');
$txt         = get_field('career_integration_txt');
$catchphrase = get_field('career_integration_catchphrase');
$img         = get_field('career_integration_img');
?>

<section class="integration">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="integration-container">
            <div class="txt">
                <p><?= $txt ?></p>
                <div class="catchphrase-container">
                    <h3><?= $catchphrase ?></h3>
                </div>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
    </div>
</section>