<?php
// Data for section
$title    = get_field('resume_your_resume_title');
$subtitle = get_field('resume_your_resume_subtitle');
$plus     = get_template_directory_uri() . '/library/images/icons/plus.svg';
$minus    = get_template_directory_uri() . '/library/images/icons/moins.svg';
?>

<section class="your-resume">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <h3><?= $subtitle ?></h3>

        <?php if (have_rows('resume_your_resume')) :
            while (have_rows('resume_your_resume')) : the_row(); ?>
            <?php
            // Data for the loop
            $tip  = get_sub_field('resume_your_resume_tip');
            $text = get_sub_field('resume_your_resume_txt');
            ?>

            <div class="your-resume-container">
                <div class="head-container">
                    <h3><?= $tip ?></h3>
                    <hr>
                    <div class="button-container">
                        <img class="plus" src="<?= $plus ?>" alt="">
                        <img class="minus invisible" src="<?= $minus ?>" alt="">
                    </div>
                </div>
                <div class="txt-container invisible">
                    <?= $text ?>
                </div>                
            </div>
        <?php endwhile;
        else :
        endif;
        ?>
    </div>
</section>