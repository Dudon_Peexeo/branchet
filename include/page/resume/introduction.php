<?php
// Data for section
$title       = get_field('resume_introduction_title');
$txt         = get_field('resume_introduction_txt');
$catchphrase = get_field('resume_introduction_catchphrase');
$img         = get_field('resume_introduction_img');
?>

<section class="introduction">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="introduction-container">
            <div class="txt">
                <p><?= $txt ?></p>
                <div class="catchphrase-container">
                    <h3><?= $catchphrase ?></h3>
                </div>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
    </div>
</section>