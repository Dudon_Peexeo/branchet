<?php
// Data for section
$title    = get_field('resume_cover_letter_title');
$txt      = get_field('resume_cover_letter_txt');
$subtitle = get_field('resume_cover_letter_subtitle');
$img      = get_field('resume_cover_letter_img');
?>

<section class="cover-letter">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <p><?= $txt ?></p>
        <h3><?= $subtitle ?></h3>

        <div class="cover-letter-container">
            <div class="tips-container">
                <?php if (have_rows('resume_cover_letter_tips')) : ?>
                    <?php while (have_rows('resume_cover_letter_tips')) : the_row(); ?>
                        <div class="tip-container">
                            <div class="number-cotainer">
                                <span><?= get_sub_field('resume_cover_letter_tip_number'); ?></span>
                            </div>                            
                            <p><?= get_sub_field('resume_cover_letter_tip_title'); ?></p>
                        </div>
                <?php endwhile; endif;?>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
    </div>
</section>