<?php
// Data for section
$title = get_field('why_join_opteven_committed_company_title');
$plus  = get_template_directory_uri() . '/library/images/icons/plus.svg';
$minus = get_template_directory_uri() . '/library/images/icons/moins.svg';
?>

<section class="committed-company">
    <div class="wrap">
        <h2><?= $title ?></h2>

        <?php if (have_rows('why_join_opteven_committed_company')) : ?>
            <?php while (have_rows('why_join_opteven_committed_company')) : the_row(); ?>
            <?php
            // Data for the loop
            $icon     = get_sub_field('why_join_opteven_committed_company_icon');
            $subtitle = get_sub_field('why_join_opteven_committed_company_subtitle');
            $text     = get_sub_field('why_join_opteven_committed_company_txt');
            ?>

            <div class="engagements-container">
                <div class="head-container">
                    <div class="title">
                        <img src="<?= $icon['url'] ?>" alt="">
                        <h3><?= $subtitle ?></h3>
                    </div>  
                    <hr>
                    <div class="button-container">
                        <img class="plus" src="<?= $plus ?>" alt="">
                        <img class="minus invisible" src="<?= $minus ?>" alt="">
                    </div>
                </div>
                <div class="txt-container invisible">
                    <p><?= $text ?></p>
                </div>                
            </div>

        <?php endwhile; endif;?>
    </div>
</section>