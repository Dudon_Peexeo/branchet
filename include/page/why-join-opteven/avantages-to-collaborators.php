<?php
// Data for section
$title    = get_field('why_join_opteven_avantages_to_collaborators_title');
$subtitle = get_field('why_join_opteven_avantages_to_collaborators_subtitle');
?>

<section class="avantages-to-collaborators">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <h3><?= $subtitle?></h3>

        <?php if (have_rows('why_join_opteven_avantages_to_collaborators')) : ?>
            <?php while (have_rows('why_join_opteven_avantages_to_collaborators')) : the_row(); ?>

            <div class="avantages-to-collaborators-container">
                <img src="<?= get_sub_field('img') ?>" alt="">
                <div class="entry-content">
                    <h3><?= get_sub_field('title') ?></h3>
                    <div class="txt">
                        <p><?= get_sub_field('txt') ?></p>
                    </div>
                </div>
            </div>
        <?php endwhile; endif;?>                        

    </div>
</section>