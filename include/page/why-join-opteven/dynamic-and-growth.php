<?php
// Data for section
$title       = get_field('why_join_opteven_dynamic_and_growth_title');
$txt         = get_field('why_join_opteven_dynamic_and_growth_txt');
$catchphrase = get_field('why_join_opteven_dynamic_and_growth_catchphrase');
$img         = get_field('why_join_opteven_dynamic_and_growth_img');
?>

<section class="dynamic-and-growth">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="dynamic-and-growth-container">
            <div class="txt">
                <p><?= $txt ?></p>
                <div class="catchphrase-container">
                    <h3><?= $catchphrase ?></h3>
                </div>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
    </div>
</section>