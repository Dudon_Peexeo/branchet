<?php
// Data for section
$title  = get_field('why_join_opteven_listening_to_collaborators_title');
$img    = get_field('why_join_opteven_listening_to_collaborators_img');
$txt    = get_field('why_join_opteven_listening_to_collaborators_txt');
$button = get_field('why_join_opteven_listening_to_collaborators_button');
?>

<section class="listening-to-collaborators">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="listening-to-collaborators-container">
            <img src="<?= $img ?>" alt="">

            <div class="txt-container">
                <div class="txt">
                    <p><?= $txt ?></p>
                </div>
                <a class="button-link" href="<?= $button['url'] ?>"><?= $button['txt'] ?></a>
            </div>
        </div>
    </div>
</section>