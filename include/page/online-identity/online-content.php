<?php
// Data for section
$catchphrase = get_field('online_content_catchphrase');
?>

<section class="online-content">
    <div class="wrap">
        <h3><?= $catchphrase ?></h3>
        <div class="online-content-container">

            <?php if (have_rows('online_content')) : ?>
            <?php while (have_rows('online_content')) : the_row(); ?>
            <?php
            // Data for the loop
            $title = get_sub_field('title');
            $img   = get_sub_field('img');
            $txt   = get_sub_field('txt');

            ?>
                <div class="box entry-content">
                    <h2><?= $title ?></h2>
                    <div class="box-content">
                        <?php if ($img) : ?>
                            <img src="<?= $img ?>" alt="">
                        <?php endif ?>
                        <div class="txt-container">
                            <p><?= $txt ?></p>
                        </div>
                    </div>
                </div>

            <?php endwhile; endif;?>
        </div>
    </div>
</section>