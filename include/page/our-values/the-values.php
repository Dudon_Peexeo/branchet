<?php
// Data for section
$title    = get_field('our_values_page_title');
$subtitle = get_field('our_values_page_subtitle');
?>

<section class="the-values">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <h3><?= $subtitle ?></h3>

        <div class="box-values-container">
            <?php if (have_rows('our_values_icons')): while (have_rows('our_values_icons')): the_row(); ?>
                <?php
                // Data for the loop
                $img = get_sub_field('our_values_image');
                $txt = get_sub_field('our_values_txt');
                ?>

                <a href="#<?= $img['title'] ?>" class="box-values">
                    <img src="<?= $img['url'] ?>" alt="">
                    <h3><?= $txt ?></h3>
                </a>
            <?php endwhile; endif;?>
        </div>
    </div>
</section>