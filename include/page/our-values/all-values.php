<?php if (have_rows('our_values')): while (have_rows('our_values')): the_row(); ?>
<?php
    $img    = get_sub_field('our_values_img');
    $icon   = get_sub_field('our_values_icon');
    $title  = get_sub_field('our_values_title');
    $text   = get_sub_field('our_values_txt');
?>

    <section id="<?= $icon['title'] ?>" class="value">
        <div class="wrap">
            <div class="value-container">
                <img src="<?= $img ?>" alt="">
                <div class="txt-container">  
                    <div class="title-container">
                        <img src="<?= $icon['url'] ?>" alt="">
                        <h3><?= $title ?></h3>
                    </div>    
                    <p><?= $text ?></p>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; endif;?>                        
