<section class="the-process-illustration">
    <div class="wrap">
        <div class="the-process-illustration-container">

            <?php if (have_rows('the_process_illustration')):
                while (have_rows('the_process_illustration')): the_row(); ?>
                <?php
                // Data for section
                $icon = get_sub_field('the_process_illustration_icon');
                $title  = get_sub_field('the_process_illustration_title');
                ?>

                <a href="#<?= $icon['title'] ?>" class="box-values">
                    <img src="<?= $icon['url'] ?>" alt="">
                    <h3><?= $title ?></h3>
                </a>

            <?php endwhile; endif;?>

        </div>
    </div>
</section>