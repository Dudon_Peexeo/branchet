<?php if (have_rows('the_process')): while (have_rows('the_process')): the_row(); ?>
<?php
    $img   = get_sub_field('the_process_img');
    $icon  = get_sub_field('the_process_icon');
    $title = get_sub_field('the_process_title');
    $text  = get_sub_field('the_process_txt');
?>

    <section id="<?= $icon['title'] ?>" class="the-process">
        <div class="wrap">
            <div class="the-process-container">
                <img src="<?= $img ?>" alt="">
                <div class="txt-container">  
                    <div class="title-container">
                        <img src="<?= $icon['url'] ?>" alt="">
                        <h3><?= $title ?></h3>
                    </div>    
                    <p><?= $text ?></p>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; endif;?>                        
