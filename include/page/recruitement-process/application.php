<?php
// Data for section
$title = get_field('recruitement_process_application_title');
?>

<section class="application">
    <div class="wrap">
        <h2><?= $title ?></h2>
        <div class="application-container">

            <?php if (have_rows('recruitement_process_application')) : ?>
            <?php while (have_rows('recruitement_process_application')) : the_row(); ?>
            <?php
            // Data for the loop
            $title = get_sub_field('title');
            $img   = get_sub_field('img');
            $link  = get_sub_field('link')

            ?>
                <div class="box">
                    <h3><?= $title ?></h3>
                    <img src="<?= $img ?>" alt="">
                    <a href="<?= $link['url'] ?>" class="button-link"><?= $link['txt'] ?></a>
                </div>

            <?php endwhile; endif;?>
        </div>
    </div>
</section>