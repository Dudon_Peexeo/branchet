<?php
// Data for section
$title = get_field('during_the_interview_title');
$img   = get_field('during_the_interview_img');
$txt   = get_field('during_the_interview_txt');
?>

<section class="during-the-interview">
    <div class="wrap">
        <h2><?= $title ?></h2> 
        <div class="during-the-interview-container-01">
            <img src="<?= $img ?>" alt="">
            <div class="txt-container">
                <p><?= $txt['txt_01'] ?></p>
            </div>
        </div>
        <div class="during-the-interview-container-02">
            <p><?= $txt['txt_02'] ?></p>
        </div>
    </div>
</section>