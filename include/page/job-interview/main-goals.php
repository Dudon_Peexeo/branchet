<?php
// Data for section
$catchphrase = get_field('job_interview_goals_catchphrase');
$title       = get_field('job_interview_goals_title');
?>

<section class="main-goals">
    <div class="wrap">
        <h3><?= $catchphrase ?></h3>
        <h2><?= $title ?></h2>

        <div class="goals-container">
            <?php if (have_rows('job_interview_goals')) : ?>
                <?php while (have_rows('job_interview_goals')) : the_row(); ?>
                    <div class="goal-container-title">
                        <div class="number-cotainer">
                            <span><?= get_sub_field('number'); ?></span>
                        </div>                            
                        <h3><?= get_sub_field('title'); ?></h3>
                    </div>
                    <p><?= get_sub_field('txt'); ?></p>
            <?php endwhile; endif;?>
        </div>
    </div>
</section>