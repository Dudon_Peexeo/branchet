<?php
// Data for section
$img      = get_field('job_interview_illustration_img');
$txt      = get_field('job_interview_illustration_txt');
?>

<section class="illustration">
    <div class="wrap">        
        <div class="illustration-container">
            <img src="<?= $img ?>" alt="">
            <div class="txt-container">
                <p><?= $txt ?></p>
            </div>
        </div>
    </div>
</section>