<?php
    /*
    *  Query posts for a relationship value.
    *  Retrieve the CPT Testimony for the current job
    */

    $testimonys = get_posts(array(
        'post_type' => 'testimony',
        'meta_query' => array(
            array(
                'key' => 'relation_jobs_testimony', // name of custom field
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE'
            )
        )
    ));
?>
<?php if ($testimonys): ?>
<section class="slider-testimony">
    <div class="wrap">
        <div class="title-testimony">
            <h2><?php _e('Nos métiers racontés par nos collaborateurs', 'opteven') ?></h2>
            <a href="/temoignages" class="button-link"><?php _e('Tous les témoignages', 'opteven'); ?></a>
        </div>
        <ul class="slides-jobs-testimony">
            <?php foreach ($testimonys as $testimony): ?>
                <li class="slide">
                    <div class="img-container">
                        <img src="<?= get_the_post_thumbnail_url($testimony->ID, 'full');?>" alt="">
                    </div>
                    <div class="the-content">
                        <!-- Display the testimony -->
                        <?= get_post($testimony->ID)->post_content; ?>
                        <!-- Display the testimony author -->
                        <div class="wp-signature">
                            <p><?= get_the_title($testimony->ID);?></p>
                        </div>
                        <!-- Display the testimony author's job -->
                        <div class="jobs">
                            <h5><?= get_field('job_title')?></h5>
                        </div>
                        <div class="dots-jobs-testimony"></div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>
<?php endif; ?>