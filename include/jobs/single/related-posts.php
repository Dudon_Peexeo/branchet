<?php
    /*
    *  Query posts for a related CPT.
    *  Retrieve all the CPT related to the current job
    */

    //get the taxonomy terms of custom post type
    $customTaxonomyTerms = wp_get_object_terms($post->ID, 'custom_cat_jobs', array('fields' => 'ids'));

    //query arguments
    $args = array(
        'post_type' => 'jobs',
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'custom_cat_jobs',
                'field' => 'id',
                'terms' => $customTaxonomyTerms
            )
        ),
        'post__not_in' => array($post->ID),
    );

    //the query
    $relatedPosts = new WP_Query($args);
?>

<?php if ($relatedPosts->have_posts()) : ?>
    <section class="related-services">
        <div class="wrap">
            <div class="related-title">
                <h2><?php _e('Les métiers du même service', 'opteven') ?></h2>
                <a class="button-link" href="/nos-metiers"><?php _e('Tous les métiers', 'opteven') ?></a>
            </div>
            <div class="jobs-container">
                <?php while ($relatedPosts->have_posts()) : $relatedPosts->the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="job-single">
                        <h3><?php the_title(); ?></h3>
                        <span class="button-link-small" href="<?php the_permalink(); ?>"><?php _e('Voir plus', 'opteven') ?></span>
                    </a>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; wp_reset_postdata(); ?>