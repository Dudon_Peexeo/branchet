<!-- Get the terms for category jobs -->
<?php
    $terms = get_terms('custom_cat_jobs');
?>

<?php if ($terms) : foreach ($terms as $term) : ?>

<!-- Set the query for CPT jobs -->
<?php
    $args = array(
    'post_type' => 'jobs',
    'tax_query' => array(
        array(
            'taxonomy' => 'custom_cat_jobs',
            'terms' => array($term->term_id),
            'include_children' => true,
            'operator' => 'IN',
            )
        )
    );
    
    $jobs = new WP_Query($args);
?>

    <?php if ($jobs->have_posts()) : ?>
        <section id="<?= $term->slug;?>" class="services-jobs">
            <div class="wrap">  
                <h2><?= $term->name;?></h2>
                <div class="category-container">
                    <img class="img-container" src="<?= z_taxonomy_image_url($term->term_id); ?>">
                    <div class="jobs-container">     
                        <?php while ($jobs->have_posts()) : $jobs->the_post(); ?>
                            <a href="<?php the_permalink(); ?>" class="job-single">
                                <h3><?php the_title(); ?></h3>
                                <span class="button-link-small" href="<?php the_permalink(); ?>"><?= $see_more ?></span>
                            </a>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>        
        </section>
    <?php endif ?>

    <?php endforeach; endif; ?>
<?php wp_reset_query(); ?>

