<section class="search-bar">
    <div class="wrap">
        <h3 class="title-jobs">
            <?= $page_title ?>
        </h3>
        <div class="search-background">
            <div class="search-container">

                <?php
                // Get the terms from category jobs
                $terms = get_terms('custom_cat_jobs');

                // Create two empty arrays : one for the name, one for the slug
                $term_name = array();
                $term_slug = array();

                // Push the terms in each associated array
                foreach ($terms as $term) {
                    array_push($term_name, $term->name);
                    array_push($term_slug, $term->slug);
                }

                // Sort the arrays in alphabetical order
                sort($term_name); sort($term_slug);
                ?>
                
                <select id="anchortoselect" class="select-jobs">
                    <option selected="" disabled=""><?= $search_bar ?></option>
                    <?php foreach (array_combine($term_slug, $term_name) as $term_slug => $term_name) : ?>
                        <option data-href="#<?= $term_slug ?>">
                            <?= $term_name ?>
                        </option>
                    <?php endforeach; ?>
                </select>

                <span class="button-search"><?= $button ?></span>
            </div>
        </div>
    </div>
</section>
