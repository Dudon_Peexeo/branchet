<section class="speciality">
    <div class="wrap">
        <h2 class="main-title"><?= _e('Nos spécialités', 'Branchet') ?></h2>

        <div class="speciality-container">

        <!-- Get the terms for category speciality -->
        <?php $terms = get_terms('custom_cat_speciality'); ?>

        <?php if ($terms) : foreach ($terms as $term) : ?>
        <?php
            $args = array(
            'post_type' => 'speciality',
            'orderby'   => 'title',
            'order'     => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'custom_cat_speciality',
                    'terms'    => array($term->term_id),
                    )
                )
            );
            
            $speciality = new WP_Query($args);
        ?>

        <?php if ($speciality->have_posts()) : ?>
            <div class="<?= $term->slug;?>">
                <h3><?= $term->name;?></h3>
                <div class="category-container">     
                    <?php while ($speciality->have_posts()) : $speciality->the_post(); ?>                        
                        <a href="<?php the_permalink(); ?>">
                            <?php include(get_template_directory() . '/library/images/icons/more-small.svg'); ?>
                            <?php the_title(); ?>
                            <?php include(get_template_directory() . '/library/images/icons/icon.arrow-right.svg'); ?>
                        </a>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif ?>

        <?php endforeach; endif; wp_reset_query(); ?>

        </div>
    </div>
</section>


