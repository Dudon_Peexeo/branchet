<?php
    // Posts loop
    $args = array(
        'orderby'        => 'date',
        'order'          => 'DSC',
        'posts_per_page' => 4
    );
    
    $blog = new WP_Query($args);
?>

<section class="blog">
    <div class="wrap">
        <h2 class="main-title"><?= _e('Nos actualités', 'Branchet') ?></h2>
        <div class="blog-container">
            <?php if ($blog->have_posts()) : ?>           
                <?php while ($blog->have_posts()) : $blog->the_post(); ?>
                    <?php
                        // Get the post thumbnail
                        $img = get_the_post_thumbnail_url(get_the_ID(), 'full');
                        // Get the post date
                        $date = get_the_date('d/m/Y');
                        // Get the post title
                        $title = get_the_title();
                        // Get the post first lines
                        $content = wp_trim_words(get_the_content(), 30);
                        // Get the post permalink
                        $link = get_permalink($post->ID);
                    ?>

                    <div class="single-container">  
                        <div class="category-container">
                            <?php
                                // Get the post categories
                                $categories = wp_get_post_categories(get_the_ID());
                                foreach ($categories as $c) {
                                    $category = get_category($c);
                                    echo '<h4>' . $category->name . '</h4>';
                                };
                            ?>
                        </div>
                        <a href="<?= $link ?>">
                            <div class="img-container">
                                <img src="<?= $img ?>" alt="">
                                <div class="middle">
                                    <?php include(get_template_directory() . '/library/images/icons/more-big.svg'); ?>
                                </div>
                            </div> 
                        </a>
                        <h4><?= $date ?></h4>
                        <h3><?= $title ?></h3>
                        <p><?= $content ?></p>
                        <a class="btn-blue-hover-dark-blue see-more" href="<?= $link ?>"><?= _e('Lire la suite', 'Branchet') ?></a>
                    </div>
                <?php endwhile; ?>
             <?php endif; wp_reset_query() ?>
        </div>
    </div>
</section>


