<?php
$home_header_slogan = get_field('home_header_slogan');
$home_header_link   = get_field('home_header_link');
$home_header_img    = get_field('home_header_img');
?>

<section class="home-header">
    <div class="wrap">
        <div class="wrap-content">
            <div class="header_section_txt">
                <?php if ($home_header_slogan) : ?>
                    <h1>
                        <?= $home_header_slogan ?>
                    </h1>
                <?php endif; ?>
                <?php if ($home_header_link) : ?>
                    <div class="button-section">
                        <a class="button-link btn-blue-hover-white" href="<?php echo $home_header_link['url'] ?>">
                            <?php echo $home_header_link['txt'] ?>
                        </a>
                    </div>
                <?php endif ?>
            </div>
            <?php if ($home_header_img) : ?>
                <div class="header_section_image">
                    <div class="header_section_image_logo"></div> <img src="<?= $home_header_img['url'] ?>">
                </div>
            <?php endif ?>
        </div>
    </div>
</section>