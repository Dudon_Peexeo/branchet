<section class="infos">
    <div class="wrap">
        <div class="infos-container">

            <?php if (have_rows('home_infos')) : ?>
                <?php while (have_rows('home_infos')) : the_row(); ?>
                    <?php
                    // Data for the loop
                    $title = get_sub_field('title');
                    $img   = get_sub_field('img');
                    $txt   = get_sub_field('txt');
                    $link  = get_sub_field('link');
                    ?>
                    <div class="infos-box">
                        <h4><?= $title ?></h4>
                        <div class="content-container">
                            <?php if ($img) : ?>
                                <img src="<?= $img ?>" alt="">
                            <?php endif; ?>
                            <?php if ($txt) : ?>
                                <p><?= $txt ?></p>
                            <?php endif; ?>
                            <a class="btn-blue-hover-dark-blue" href="<?= $link['url'] ?>"><?= $link['txt'] ?></a>
                        </div>
                    </div>                    
            <?php endwhile;
            endif; ?>

        </div>
    </div>
</section>