<?php
// Data for section
$title = get_field('home_benefit_title');
?>

<section class="benefit">
    <div class="wrap">
        <div class="wrap-content">
            <div class="benefit-title">
                <h3><?= $title ?></h3>
            </div>
            <div class="application-container">

                <?php if (have_rows('home_benefit')) : ?>
                    <?php while (have_rows('home_benefit')) : the_row(); ?>
                        <?php
                        // Data for the loop
                        $title = get_sub_field('txt');
                        $img   = get_sub_field('img');
                        ?>
                        <div class="box">
                            <div class="container-img">
                                <?= file_get_contents($img['url']) ?>
                            </div>                            
                            <p><?= $title ?></p>
                        </div>
                <?php endwhile;
                endif; ?>
            </div>
        </div>
    </div>
</section>