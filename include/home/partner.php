<section class="partner">
    <div class="wrap">
        <?php if (have_rows('home_partner')) : ?>

            <ul id="partner-slides slider" class="partner-slides">
                
                <?php while (have_rows('home_partner')) : the_row(); $img = get_sub_field('img'); ?>

                    <li class="slide">
                        <img src="<?= $img ?>" alt="">
                    </li>
                            
                <?php endwhile;?>
            
            </ul>

        <div class="dots-header"></div>

        <?php endif; ?>
    </div>
</section>