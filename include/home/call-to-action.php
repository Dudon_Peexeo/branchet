<?php if (have_rows('home_cta')) : ?>
    <?php $num = 1 ?>
    <?php while (have_rows('home_cta')) : the_row(); ?>
        <section class="cta-<?= $num ?>">

            <?php
            // Data for the loop
            $title = get_sub_field('title');
            $txt   = get_sub_field('txt');
            $link  = get_sub_field('link');
            ?>
            <div class="wrap">
                <div class="cta-container">
                    <div class="cta-txt">
                        <!-- Titre -->
                        <h2><?= $title ?></h2>

                        <!-- Zone de texte -->
                        <?php if ($txt) : ?>
                            <p><?= $txt ?> </p>
                        <?php endif ?>

                        <!-- Lien -->
                        <?php if ($link) : ?>
                            <div class="cta-button">
                                <a class="btn-blue-medium-hover-dark" href="<?= $link['url'] ?>">
                                    <span class="img-btn"></span> <?= $link['txt'] ?> 
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>  
                    <?php if ($num == 1) : ?>
                        <div class="cta-img">
                            <img src="<?= get_template_directory_uri(); ?>/library/images/pages/home/internes-offres.png">
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php $num++ ?>
<?php endwhile;
endif; ?>
