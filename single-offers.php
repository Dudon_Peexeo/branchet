<?php get_header(); ?>

<!-- Set the query offers  -->
<?php
    $args = array(
        'post_type' => 'offers',
        'posts_per_page' => 3,
        'orderby' => 'date',
        'order' => 'DESC',
        'post__not_in' => array($post->ID),
    );
    $query_offers = new WP_Query($args);
?>

<main id="main" class="main">

    <section class="header-top">
        <div class="wrap">
            <nav aria-label="Breadcrumb" >
                <?php custom_breadcrumbs(); ?>
            </nav>
            <h1>
                <?php $title = the_title(); ?>
            </h1>		
        </div>
    </section>

    <!-- Single Offer Content -->
    <section class="offer-content">
        <div class="wrap">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <section class="header-offer">
                    <div>
                        <p><?php _e('Type de contrat', 'opteven');?></p>
                        <p><?php the_field('vac_contract'); ?></p>
                    </div>
                    <div>
                        <p><?php _e('Localisation', 'opteven');?></p>
                        <p><?php the_field('vac_town'); ?></p>
                    </div>
                    <div>
                        <p><?php _e('Date de l’offre', 'opteven');?></p>
                        <p><?php the_field('vacancy_activation_date'); ?></p>
                    </div>
                </section>
                <section class="entry-content">
                    <?php the_field('texte_dintroduction', 'option'); ?>
                    <?php if(get_field('vaclang_contexte')): ?>
                        <h2><?php _e('Vos missions', 'opteven');?></h2>
                        <?php the_field('vaclang_contexte'); ?>
                    <?php endif; ?>
                    <?php if(get_field('vaclang_profil')): ?>
                        <h2><?php _e('Profil recherché', 'opteven');?></h2>
                        <?php the_field('vaclang_profil'); ?>
                    <?php endif; ?>
                    <?php if(get_field('vaclang_offer')): ?>
                        <h2><?php _e('Conditions de travail', 'opteven');?></h2>
                        <?php the_field('vaclang_offer'); ?>
                    <?php endif; ?>
                </section>
                <section class="buttons-bottom">
                    <!-- <a href="<?php the_field('application_link'); ?>" class="button-link" target="_blank"><?php _e('Postuler', 'opteven');?></a> -->
                    <a href="<?php echo get_home_url(); ?>/postuler/?offerID=<?php the_field('vacancy_id'); ?>&title=<?php the_title(); ?>" class="button-link"><?php _e('Postuler', 'opteven');?></a>
                    <div class="share-buttons">
                        <?php echo do_shortcode('[addtoany]'); ?>
                    </div>
                </section>
                <section class="more-offer">
                    <div class="title-more-offer">
                        <h2><?php _e('Plus d’offres d’emploi', 'opteven');?></h2>
                        <a href="<?php echo get_home_url(); ?>/nos-offres" class="button-link"><?php _e('Toutes nos offres', 'opteven');?></a>
                    </div>
                    <div class="wrap-content-offers">
                        <?php if ($query_offers->have_posts()) : while ($query_offers->have_posts()) : $query_offers->the_post();?>
                            <a href="<?php the_permalink(); ?>" class="single-offer">
                                <span class="single-offer-content">
                                    <span class="contract"><?php the_field('vac_contract') ?></span>
                                    <h3><?= the_title(); ?></h3>
                                    <span class="grp">
                                        <span class="town"><?php the_field('vac_town') ?></span>
                                        <span class="date"><?php the_field('vacancy_activation_date') ?></span>
                                    </span>
                                </span>
                                <span class="button-link-small"><?php _e('Consulter', 'opteven') ?></span>
                            </a>
                        <?php endwhile; endif; wp_reset_postdata();?>
                        <?php if (have_rows('home_offers')): ?>
                            <?php while (have_rows('home_offers')): the_row(); ?>
                                <?php $link = get_sub_field('link'); ?>
                                <a href="<?= $link['url'] ?>" class="single-offer last">
                                    <span class="button-link"><?= $link['txt'];?></span>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </section>
            <?php endwhile; ?>

            <?php else : ?>

                <?php include(get_template_directory() . '/components/post/post-not-found.php'); ?>

            <?php endif; ?>
        </div>
    </section>

</main>

<?php get_footer(); ?>
