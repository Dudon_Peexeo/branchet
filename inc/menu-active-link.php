<?php

/**
 * Add HTML suffix to nav active items in the main nav
 * source : https://gist.github.com/access42/ef9b517046b985971754086c4b937ce3
 */
class a42_active_item_walker extends Walker_Nav_Menu
{
	// Function responsible for the opening <li> tag and the link
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		$is_current_item = '';
		$is_current_item_class = '';

		// Replace 'twentynineteen' by your own text domain
		global $text_domain; if (!$text_domain && !isset($text_domain)) $text_domain = wp_get_theme();

		// Get item classes
		$total = count($item->classes);
		$i = 0;

		// Loop through each class to display them accordingly
		foreach ($item->classes as $item_class){
			$is_current_item_class .= $item_class;

			// Insert white spaces if needed
			if ($i > 0 && $i != $total - 1) :
				$is_current_item_class .= ' ';
			endif;

			$i++;
		}

		// Add HTML in the active item
		if(array_search('current-menu-item', $item->classes) != 0)
		{
			$is_current_item = '<span class="sr-only">' . __(' – active page', $text_domain) . '</span>';
		}

		// Output result
		$output .= '<li class="'.$is_current_item_class.'"><a href="'.$item->url.'">'.$item->title . $is_current_item;
	}

	// Function responsible for the closing </li> tag is end_el
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</a></li>';
	}
}
