<?php
//
// Disable all colors within Gutenberg.
//
// function gutenberg_disable_all_colors() {
//     add_theme_support( 'editor-color-palette' );
//       add_theme_support( 'disable-custom-colors' );
//   }
//   add_action( 'after_setup_theme', 'gutenberg_disable_all_colors' );


// Disable and change all colors within Gutenberg.
function mytheme_setup_theme_supported_features()
{
    add_theme_support(
        'editor-color-palette',
        array(
            array(
                'name' => 'Bleu Clair',
                'slug'  => 'blue-light',
                'color' => '#00A0DF'
            ),
            array(
                'name' => 'Bleu',
                'slug'  => 'blue',
                'color' => '#004F99'
            ),
            array(
                'name' => 'Bleu medium',
                'slug'  => 'blue-medium',
                'color' => '#0D2A58'
            ),
            array(
                'name' => 'Bleu foncé',
                'slug'  => 'blue-dark',
                'color' => '#000D3C'
            ),
            array(
                'name' => 'Gris',
                'slug'  => 'grey',
                'color' => '#D0D8EB'
            ),
            array(
                'name' => 'Gris clair',
                'slug'  => 'grey-light',
                'color' => '#EFF2F8'
            ),
            array(
                'name' => 'Blanc',
                'slug'  => 'white',
                'color' => '#FFFFFF'
            ),
            array(
                'name' => 'Orange',
                'slug'  => 'orange',
                'color' => '#FFA573'
            ),
            // array(
            //     'name' => 'Bleu dégradé',
            //     'slug'  => 'blue',
            //     'color' => '#FFFFFF'
            // ),
            array(
                'name' => 'Noir',
                'slug'  => 'black',
                'color' => '#1F1F1F'
            ),
        )
    );

    // add_theme_support('editor-gradient-presets', array(
    //     array(
    //         'name' => __('Light blue to white'),
    //         'gradient' => 'linear-gradient(180deg, rgba(0,101,155,0.5) 0%, rgba(255,255,255,1) 100%)',
    //         'slug' => 'light-blue-to-white'
    //     ),
    // ));

    // Disable Custom Colors
    add_theme_support('disable-custom-colors');
}
add_action('after_setup_theme', 'mytheme_setup_theme_supported_features');
