<?php
/**
 * Creating Blocks with ACF PRO (require plugin ACF PRO to work)
 * doc: https://www.advancedcustomfields.com/resources/acf_register_block_type/
 * https://www.advancedcustomfields.com/resources/blocks/#getting-started
*/

function register_acf_block_testimonials()
{

    // register a testimonial block.
    acf_register_block_type(array(
        'name'              => 'testimonial',
        'title'             => __('Testimonial'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => '/components/acf-blocks/testimonial.php',
        'enqueue_style'     => get_template_directory_uri() . '/library/css/acf-block.css',
        'category'          => 'common',
        'icon'              => 'testimonial',
        'keywords'          => array( 'testimonial', 'quote' ),
        'anchor'            => 'id',
        'className'         => 'wp-testimonial',
        'mode'              => 'edit',
        'post_types'        => array('post', 'page'),
        'supports'          => array(
        'align'             => false,
        ),
    ));
}

function register_acf_block_signature()
{
    // register a testimonial block.
    acf_register_block_type(array(
        'name'              => 'signature',
        'title'             => __('Signature'),
        'description'       => __('A custom expert block.'),
        'render_template'   => '/components/acf-blocks/signature.php',
        'enqueue_style'     => get_template_directory_uri() . '/library/css/acf-block.css',
        'category'          => 'common',
        'icon'              => 'welcome-write-blog',
        'keywords'          => 'signature',
        'anchor'            => 'id',
        'className'         => 'wp-signature',
        'mode'              => 'edit',
        'post_types'        => array('post', 'page'),
        'supports'          => array(
        'align'             => false,
        ),
    ));
}

function register_acf_block_expert()
{
    // register a testimonial block.
    acf_register_block_type(array(
        'name'              => 'expert',
        'title'             => __('Expert'),
        'description'       => __('A custom block.'),
        'render_template'   => '/components/acf-blocks/expert.php',
        'enqueue_style'     => get_template_directory_uri() . '/library/css/acf-block.css',
        'category'          => 'common',
        'icon'              => 'align-center',
        'keywords'          => 'expert',
        'anchor'            => 'id',
        'className'         => 'wp-expert',
        'mode'              => 'edit',
        'post_types'        => array('speciality'),
        'supports'          => array(
            'align'             => false,
        ),
    ));
}

// Check if function exists and hook into setup.
if (function_exists('acf_register_block_type')) {
    add_action('acf/init', 'register_acf_block_testimonials');
    add_action('acf/init', 'register_acf_block_signature');
    add_action('acf/init', 'register_acf_block_expert');
}
