<?php

// Gutenberg scripts and styles
// source: https://www.billerickson.net/wordpress-color-palette-button-styling-gutenberg/
function gutenberg_scripts()
{
    wp_enqueue_script('gutenberg-script', get_stylesheet_directory_uri() . '/library/js/gutenberg.js', array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ), filemtime(get_stylesheet_directory() . '/library/js/gutenberg.js'));
}
add_action('enqueue_block_editor_assets', 'gutenberg_scripts');

// removing drop cap from p (not the best but today 20/02/2020 the only way)
function biro_gutenberg_remove_drop_cap()
{
    echo '<style>.blocks-font-size .components-base-control:first-of-type { margin-bottom: 0; } .blocks-font-size .components-toggle-control { display: none; }</style>';
}
add_action('admin_head', 'biro_gutenberg_remove_drop_cap');
