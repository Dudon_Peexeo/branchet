<?php
// activate and deactivate gutenberg blocks
// snippet that allows you to keep the custom plugins blocks!
// https://rudrastyh.com/gutenberg/remove-default-blocks.html#block_slugs
add_filter('allowed_block_types', 'keep_plugins_blocks');

function keep_plugins_blocks($allowed_blocks)
{
    // get widget blocks and registered by plugins blocks
    $registered_blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();

    // in case we do not need widgets
    unset($registered_blocks[ 'core/latest-comments' ]);
    unset($registered_blocks[ 'core/archives' ]);
    unset($registered_blocks[ 'core/categories' ]);
    unset($registered_blocks[ 'core/latest-posts' ]);
    unset($registered_blocks[ 'core/shortcode' ]);
    unset($registered_blocks[ 'core/code' ]);
    unset($registered_blocks[ 'core/rss' ]);
    unset($registered_blocks[ 'core/calendar' ]);
    unset($registered_blocks[ 'core/search' ]);
    unset($registered_blocks[ 'core/tag-cloud' ]);

    // now $registered_blocks contains only blocks registered by plugins,
    // but we need keys only
    $registered_blocks = array_keys($registered_blocks);

    // merge the whitelist with plugins blocks
    // list of the blocks: https://rudrastyh.com/gutenberg/remove-default-blocks.html#block_slugs
    return array_merge(array(
        'core/image',
        'core/paragraph',
        'core/quote',
        'core/heading',
        'core/list',
        'core/button',
        'core/columns',
        'core/media-text',
        'core/table',
        'core/group',
        // 'core/gallery',
        // 'core/audio',
        // 'core/cover',
        // 'core/file',
        // 'core/video',
        // 'core/verse',
        // 'core/code',
        // 'core/freeform',
        // 'core/html',
        // 'core/preformatted',
        // 'core/pullquote',
        // 'core/shortcode',
        // 'core/archives',
        // 'core/categories',
        // 'core/latest-comments',
        // 'core/latest-posts',
        // 'core/calendar',
        // 'core/rss',
        // 'core/search',
        // 'core/tag-cloud',
        // 'core/embed',
        // 'core-embed/twitter',
        // 'core-embed/youtube',
        // 'core-embed/facebook',
        // 'core-embed/instagram',
        // 'core-embed/wordpress',
        // 'core-embed/soundcloud',
        // 'core-embed/spotify',
        // 'core-embed/flickr',
        // 'core-embed/vimeo',
        // 'core-embed/animoto',
        // 'core-embed/cloudup',
        // 'core-embed/collegehumor',
        // 'core-embed/dailymotion',
        // 'core-embed/funnyordie',
        // 'core-embed/hulu',
        // 'core-embed/imgur',
        // 'core-embed/issuu',
        // 'core-embed/kickstarter',
        // 'core-embed/meetup-com',
        // 'core-embed/mixcloud',
        // 'core-embed/photobucket',
        // 'core-embed/polldaddy',
        // 'core-embed/reddit',
        // 'core-embed/reverbnation',
        // 'core-embed/screencast',
        // 'core-embed/scribd',
        // 'core-embed/slideshare',
        // 'core-embed/smugmug',
        // 'core-embed/speaker',
        // 'core-embed/ted',
        // 'core-embed/tumblr',
        // 'core-embed/videopress',
        // 'core-embed/wordpress-tv',
    ), $registered_blocks);
}
