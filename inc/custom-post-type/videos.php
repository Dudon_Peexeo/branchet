<?php

    function custom_post_videos()
    {
        $labels = array(
            'name'               => __('Vidéos', 'bonestheme'),
            'singular_name'      => __('Custom Post', 'bonestheme'),
            'all_items'          => __('Tout', 'bonestheme'),
            'add_new'            => __('Ajouter', 'bonestheme'),
            'add_new_item'       => __('Ajouter une nouvelle', 'bonestheme'),
            'edit'               => __('Éditer', 'bonestheme'),
            'edit_item'          => __('Éditer', 'bonestheme'),
            'new_item'           => __('Nouveau', 'bonestheme'),
            'view_item'          => __('Afficher', 'bonestheme'),
            'search_items'       => __('Chercher', 'bonestheme'),
            'not_found'          => __('Rien trouvé dans la base de données', 'bonestheme'),
            'not_found_in_trash' => __('Rien trouvé dans la corbeille', 'bonestheme'),
        );

        $args = array(
            'exclude_from_search' => false,
            'labels'              => $labels,
            'public'              => true,
            'capability_type'     => 'post',
            'publicly_queryable'  => true,
            'show_in_rest'        => true,
            'show_ui'             => true,
            'query_var'           => true,
            'menu_position'       => 6,
            'menu_icon'           => 'dashicons-format-video',
            'rewrite'	          => array( 'slug' => 'videos', 'with_front' => false ),
            'has_archive'         => true,
            'hierarchical'        => false,
            'supports'            => array('title','editor','thumbnail'),
        );

        register_post_type('videos', $args);
    }

    // adding the function to the Wordpress init
    add_action('init', 'custom_post_videos');

    /**
     *  Register a custom taxonomy or category associated to the custom post : videos
     *
     */

    $labels = array(
        'name'              => __('Catégories', 'bonestheme'),
        'singular_name'     => __('Catégorie', 'bonestheme'),
        'search_items'      => __('Chercher', 'bonestheme'),
        'all_items'         => __('Tout(e)', 'bonestheme'),
        'parent_item'       => __('Parent(e)', 'bonestheme'),
        'parent_item_colon' => __('Parent(e) :', 'bonestheme'),
        'edit_item'         => __('Éditer', 'bonestheme'),
        'update_item'       => __('Mise à jour', 'bonestheme'),
        'add_new_item'      => __('Ajouter un(e) nouveau(elle)', 'bonestheme'),
        'new_item_name'     => __('Nouveau(elle)', 'bonestheme')
    );

    $object = array(
        'show_admin_column' => true,
        'show_ui'           => true,
        'show_in_rest'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'custom-slug' ),
        'labels'            => $labels,
    );

    $args = 'videos';

    register_taxonomy('custom_cat_videos', $args, $object);
