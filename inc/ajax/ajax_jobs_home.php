<?php 
add_action('wp_ajax_nopriv_filter', 'filter_ajax');
add_action('wp_ajax_filter', 'filter_ajax');

function filter_ajax() {
    $username = 'Nouveau Front Office'; // Login API
    $password = '26yjRaDYzvlcqHHItyxnx754'; // Mot de passe API
    $context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("$username:$password")
    )
    ));

    $baseUrl = 'https://opteven.simply-jobs.fr/';
    $field = 'vac_contract'; // Nom du champ
    $url = $baseUrl . 'rest/v2/enum/' . $field;
    
    $data = file_get_contents($url, false, $context);
    $result = json_decode($data, true);
    $resultContract = array_sort($result, '', SORT_ASC);
?>
    <?php foreach ($resultContract as $key => $value):?>
        <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
    <?php endforeach; ?>
<?php 
    die();
}

add_action('wp_ajax_nopriv_filter_poste', 'filter_poste_ajax');
add_action('wp_ajax_filter_poste', 'filter_poste_ajax');

function filter_poste_ajax() {
    $username = 'Nouveau Front Office'; // Login API
    $password = '26yjRaDYzvlcqHHItyxnx754'; // Mot de passe API
    $context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("$username:$password")
    )
    ));

    $baseUrl = 'https://opteven.simply-jobs.fr/';
    $field = 'vacancy_title'; // Nom du champ
    $url = $baseUrl . '/rest/v2/onlinevacancy?pageItems=100&fields=' . $field;
    
    $data = file_get_contents($url, false, $context);
    $result = json_decode($data, true);
    $resultTitle = [];
?>
    <?php 
    
    $resultPost = array_sort($result['data'], 'vacancy_title', SORT_ASC);
    
    foreach ($resultPost as $key => $value):?>
        <?php $vacancy_title = sanitize_title($value['vacancy_title']);?>
        <?php if(!in_array('<option value="'.$vacancy_title.'">'.$value['vacancy_title'].'</option>', $resultTitle)): ?>
            <?php $resultTitle[] =  '<option value="'.$vacancy_title.'">'.$value['vacancy_title'].'</option>' ?>
        <?php endif; ?>
    <?php endforeach; ?>

    
    <?php
    foreach ($resultTitle as $key => $value):?>
        <?php echo $value; ?>
    <?php endforeach; ?>
<?php 
    die();
}

add_action('wp_ajax_nopriv_filter_location', 'filter_location_ajax');
add_action('wp_ajax_filter_location', 'filter_location_ajax');

function filter_location_ajax() {
    $username = 'Nouveau Front Office'; // Login API
    $password = '26yjRaDYzvlcqHHItyxnx754'; // Mot de passe API
    $context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("$username:$password")
    )
    ));

    $baseUrl = 'https://opteven.simply-jobs.fr/';
    $field = 'vac_town'; // Nom du champ
    $url = $baseUrl . '/rest/v2/onlinevacancy?pageItems=100&fields=' . $field;
    
    $data = file_get_contents($url, false, $context);
    $result = json_decode($data, true);
    $resultLocations = array_sort($result['data'], 'vac_town', SORT_ASC);
    
    $resultLocation = [];
?>
    <?php foreach ($resultLocations as $key => $value):?>
        <?php if(!in_array('<option value="'.$value['vac_town'].'">'.$value['vac_town'].'</option>', $resultLocation) && $value['vac_town'] != NULL): ?>
            <?php $resultLocation[] =  '<option value="'.$value['vac_town'].'">'.$value['vac_town'].'</option>' ?>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php foreach ($resultLocation as $key => $value):?>
        <?php echo $value; ?>
    <?php endforeach; ?>
<?php 
    die();
}

?>